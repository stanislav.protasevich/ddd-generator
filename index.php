<?php

require_once 'vendor/autoload.php';

$userId = new \Templater\Domain\Entity\User\Field\UserIdField(1);
$userEmail = new \Templater\Domain\Entity\User\Field\UserEmailField('stanislav.protasevich@gmail.com');

$user = new \Templater\Domain\Entity\User\UserEntity();
$user->setId($userId);
$user->setEmail($userEmail);