<?php

foreach ($structures as $structure)
{
    global $mapping;
    global $constructParams;

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setShortDescription(implode(' ', ['Interface', 'Delete' . $structure['class'] . 'CommandInterface']));
    $docBlock->setTag(['name' => 'package', 'description' => implode('\\', [BASE_NAMESPACE, 'Domain', 'Model', $structure['class'], 'Command'])]);

    $interface = new \Zend\Code\Generator\InterfaceGenerator();
    $interface->setNamespaceName(implode('\\', [BASE_NAMESPACE, 'Domain', 'Model', $structure['class'], 'Command']));
    $interface->setDocBlock($docBlock);
    $interface->setName('Delete' . $structure['class'] . 'CommandInterface');
    $interface->addUse('ProDevZone\Common\Identifier\IdentifierInterface');

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $parts = [];
    foreach ($structure['fields'] as $parameter => $field) {
        if ($parameter != 'id') {
            continue;
        }

        $parameter = 'Identifier';

        $name = lcfirst($structure['class']) . ucfirst($parameter);
        $type = 'string';

        if (isset($field['type']) && $field['type'] !== null && $field['type'] == 'uuid') {
            $type = 'string';
        }

        $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ParamTag(lcfirst($name), [$type]));
    }
    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ReturnTag('Delete' . $structure['class'] . 'CommandInterface'));

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setName('withIdentifier');
    $method->setDocBlock($docBlock);
    $method->setStatic(true);
    $method->setReturnType('Delete' . $structure['class'] . 'CommandInterface');
    foreach ($structure['fields'] as $parameter => $field) {
        if ($parameter != 'id') {
            continue;
        }

        $parameter = 'Identifier';

        $name = lcfirst($structure['class']) . ucfirst($parameter);
        $type = 'string';

        if (isset($field['type']) && $field['type'] !== null && $field['type'] == 'uuid') {
            $type = 'string';
        }

        $parameter = new \Zend\Code\Generator\ParameterGenerator();
        $parameter->setName($name);
        $parameter->setType($type);

        $method->setParameter($parameter);
    }
    $interface->addMethodFromGenerator($method);

    foreach ($structure['fields'] as $parameter => $field) {
        $docBlock = new \Zend\Code\Generator\DocBlockGenerator();

        $type = $field['type'];

        if ($parameter != 'id') {
           continue;
        }

        $parameter = 'Identifier';
        $type = 'IdentifierInterface';

        $name = lcfirst($structure['class']) . ucfirst($parameter);

        $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ReturnTag($type));

        $method = new \Zend\Code\Generator\MethodGenerator();
        $method->setDocBlock($docBlock);
        $method->setName($name);
        $method->setReturnType($type);

        $interface->addMethodFromGenerator($method);
    }

    $file = new \Zend\Code\Generator\FileGenerator();
    $file->setBody('declare(strict_types = 1);' . PHP_EOL . PHP_EOL . $interface->generate());

    $path = implode(DIRECTORY_SEPARATOR, [DIR_SRC, 'Domain', 'Model', $structure['class'], 'Command']);

    @mkdir($path, 0777, true);

    file_put_contents($path . DIRECTORY_SEPARATOR . $interface->getName() . PHP, $file->generate());
}