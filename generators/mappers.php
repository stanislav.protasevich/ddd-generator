<?php

foreach ($structures as $structure)
{
    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setShortDescription(implode(' ', ['Class', 'Doctrine' . $structure['class'] . 'Mapper']));
    $docBlock->setTag(['name' => 'package', 'description' => implode('\\', [BASE_NAMESPACE, 'Infrastructure', 'Persistence', 'Doctrine', 'Mapper'])]);

    $class = new \Zend\Code\Generator\ClassGenerator();
    $class->setNamespaceName(implode('\\', [BASE_NAMESPACE, 'Infrastructure', 'Persistence', 'Doctrine', 'Mapper']));
    $class->setDocBlock($docBlock);
    $class->setFinal(true);
    $class->setImplementedInterfaces([implode('\\', [BASE_NAMESPACE, DOMAIN_NAME_KEY, MODEL_NAME_KEY, $structure['class'], $structure['class'] . 'Mapper' . ucfirst(INTERFACE_NAME_KEY)])]);
    $class->addUse('Doctrine\Common\Persistence\ObjectRepository');
    $class->addUse('Doctrine\ORM\EntityManagerInterface');
    $class->addUse(implode('\\', [BASE_NAMESPACE, DOMAIN_NAME_KEY, MODEL_NAME_KEY, $structure['class']]));
    $class->addUse(implode('\\', [BASE_NAMESPACE, DOMAIN_NAME_KEY, MODEL_NAME_KEY, $structure['class'] . ucfirst(INTERFACE_NAME_KEY)]));
    $class->addUse(implode('\\', [BASE_NAMESPACE, DOMAIN_NAME_KEY, MODEL_NAME_KEY, $structure['class'], $structure['class'] . 'Mapper' . ucfirst(INTERFACE_NAME_KEY)]));
    $class->setName('Doctrine'. $structure['class'] . 'Mapper');

    //Properties
    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setTag(
        (new \Zend\Code\Generator\DocBlock\Tag\VarTag(
            'entityManager',
            ['EntityManagerInterface']
        ))
    );

    $property = new \Zend\Code\Generator\PropertyGenerator();
    $property->setDocBlock($docBlock);
    $property->setName('entityManager');
    $property->setVisibility(\Zend\Code\Generator\PropertyGenerator::VISIBILITY_PRIVATE);
    $class->addPropertyFromGenerator($property);

    //Properties
    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setTag(
        (new \Zend\Code\Generator\DocBlock\Tag\VarTag(
            'repository',
            ['ObjectRepository']
        ))
    );

    $property = new \Zend\Code\Generator\PropertyGenerator();
    $property->setDocBlock($docBlock);
    $property->setName('repository');
    $property->setVisibility(\Zend\Code\Generator\PropertyGenerator::VISIBILITY_PRIVATE);
    $class->addPropertyFromGenerator($property);

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setShortDescription('Doctrine'. $structure['class'] . 'Mapper constructor');
    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ParamTag('entityManager', ['EntityManagerInterface']));

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setName('__construct');
    $method->setDocBlock($docBlock);
    $method->setParameter(['name' => 'entityManager', 'type' => 'EntityManagerInterface']);
    $_class = $structure['class'];
    $body = <<< EOT
\$this->entityManager = \$entityManager;

\$this->repository = \$this->entityManager->getRepository($_class::class);
EOT;
    $method->setBody($body);
    $class->addMethodFromGenerator($method);

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\GenericTag('@inheritdoc'));

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setDocBlock($docBlock);
    $method->setName(implode('', ['fetchAllBy']));
    $method->setParameter(['name' => 'conditions', 'type' => 'array', 'defaultValue' => []]);
    $method->setReturnType('array');
    $body = <<< EOT
return \$this->repository->findBy(\$conditions);
EOT;
    $method->setBody($body);
    $class->addMethodFromGenerator($method);

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setDocBlock($docBlock);
    $method->setName(implode('', ['fetchOneBy']));
    $method->setParameter(['name' => 'conditions', 'type' => 'array', 'defaultValue' => []]);
    $method->setReturnType('?'. $structure['class'] . 'Interface');
    $__class = lcfirst($_class);
    $_interface = $_class . 'Interface';
    $body = <<< EOT
/** @var $_interface $$__class */
$$__class = \$this->repository->findOneBy(\$conditions);

return $$__class;
EOT;
    $method->setBody($body);
    $class->addMethodFromGenerator($method);

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setName(implode('', ['create']));
    $method->setDocBlock($docBlock);
    $method->setParameter(['name' => lcfirst($structure['class']), 'type' => $structure['class'] . 'Interface']);
    $method->setReturnType('void');
    $body = <<< EOT
\$this->entityManager->persist($$__class);
\$this->entityManager->flush();
EOT;
    $method->setBody($body);
    $class->addMethodFromGenerator($method);

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setName(implode('', ['update']));
    $method->setDocBlock($docBlock);
    $method->setParameter(['name' => lcfirst($structure['class']), 'type' => $structure['class'] . 'Interface']);
    $method->setReturnType('void');
    $body = <<< EOT
\$this->entityManager->persist($$__class);
\$this->entityManager->flush();
EOT;
    $method->setBody($body);
    $class->addMethodFromGenerator($method);

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setName(implode('', ['delete']));
    $method->setDocBlock($docBlock);
    $method->setParameter(['name' => lcfirst($structure['class']), 'type' => $structure['class'] . 'Interface']);
    $method->setReturnType('void');
    $body = <<< EOT
\$this->entityManager->remove($$__class);
\$this->entityManager->flush();
EOT;
    $method->setBody($body);
    $class->addMethodFromGenerator($method);


    $file = new \Zend\Code\Generator\FileGenerator();
    $file->setBody('declare(strict_types = 1);' . PHP_EOL . PHP_EOL . $class->generate());

    $path = implode(DIRECTORY_SEPARATOR, [DIR_SRC, 'Infrastructure', 'Persistence', 'Doctrine', 'Mapper']);

    @mkdir($path, 0777, true);

    file_put_contents($path . DIRECTORY_SEPARATOR . $class->getName() . PHP, $file->generate());
}