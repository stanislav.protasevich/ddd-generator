<?php

foreach ($structures as $structure)
{
    global $mapping;
    global $constructParams;

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setShortDescription(implode(' ', ['Class', 'Create' . $structure['class'] . 'Action']));
    $docBlock->setTag(['name' => 'package', 'description' => implode('\\', [BASE_NAMESPACE, 'Api', 'Action', $structure['class']])]);

    $class = new \Zend\Code\Generator\ClassGenerator();
    $class->setFinal(true);
    $class->setNamespaceName(implode('\\', [BASE_NAMESPACE, 'Api', 'Action', $structure['class']]));
    $class->setImplementedInterfaces(['Interop\Http\ServerMiddleware\MiddlewareInterface']);
    $class->setDocBlock($docBlock);
    $class->setName('Create' . $structure['class'] . 'Action');
    $class->addUse('Fig\Http\Message\StatusCodeInterface');
    $class->addUse('Interop\Http\ServerMiddleware\DelegateInterface');
    $class->addUse('Interop\Http\ServerMiddleware\MiddlewareInterface');
    $class->addUse('Psr\Http\Message\ServerRequestInterface');
    $class->addUse('Zend\Diactoros\Response\JsonResponse');

    $params = [];

    $class->addUse(implode('\\', [BASE_NAMESPACE, 'Domain', 'Model', $structure['class'], $structure['class'] . 'RepositoryInterface']));

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setTag(
        (new \Zend\Code\Generator\DocBlock\Tag\VarTag(
            lcfirst($structure['class']) . 'Repository',
            [$structure['class'] . 'RepositoryInterface']
        ))
    );

    $property = new \Zend\Code\Generator\PropertyGenerator();
    $property->setDocBlock($docBlock);
    $property->setName(lcfirst($structure['class']) . 'Repository');
    $property->setVisibility(\Zend\Code\Generator\PropertyGenerator::VISIBILITY_PRIVATE);
    $class->addPropertyFromGenerator($property);

    $params[] = ['name' => lcfirst($structure['class']) . 'Repository', 'type' => $structure['class'] . 'RepositoryInterface'];

    $class->addUse(implode('\\', [BASE_NAMESPACE, 'Domain', 'Model', $structure['class'], 'Command', 'Handler', 'Create' . $structure['class'] . 'CommandHandler']));
    $class->addUse(implode('\\', [BASE_NAMESPACE, 'Domain', 'Model', $structure['class'], 'Command', 'Create' . $structure['class'] . 'Command']));

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setTag(
        (new \Zend\Code\Generator\DocBlock\Tag\VarTag(
            lcfirst($structure['class']) . 'CommandHandler',
            ['Create' . $structure['class'] . 'CommandHandler']
        ))
    );

    $property = new \Zend\Code\Generator\PropertyGenerator();
    $property->setDocBlock($docBlock);
    $property->setName(lcfirst($structure['class']) . 'CommandHandler');
    $property->setVisibility(\Zend\Code\Generator\PropertyGenerator::VISIBILITY_PRIVATE);
    $class->addPropertyFromGenerator($property);

    $params[] = ['name' => lcfirst($structure['class']) . 'CommandHandler', 'type' => 'Create' . $structure['class'] . 'CommandHandler'];

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setName(implode('', ['__construct']));

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setShortDescription('Create' . $structure['class'] . 'Action constructor.');
    $parts = [];
    foreach ($params as $param) {
        $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ParamTag($param['name'], [$param['type']]));

        $parameter = new \Zend\Code\Generator\ParameterGenerator();
        $parameter->setName($param['name']);
        $parameter->setType($param['type']);

        $method->setParameter($parameter);

        $parts[] = '$this->' . $param['name'] . ' = $' .$param['name'] . ';';
    }

    /* Method "process" */

    $method->setBody(implode(PHP_EOL, $parts));
    $method->setDocBlock($docBlock);
    $class->addMethodFromGenerator($method);

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ParamTag('request', ['ServerRequestInterface']));
    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ParamTag('delegate', ['DelegateInterface']));
    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ReturnTag('JsonResponse'));

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setDocBlock($docBlock);
    $method->setName(implode('', ['process']));
    $method->setParameter(['name' => 'request', 'type' => 'ServerRequestInterface']);
    $method->setParameter(['name' => 'delegate', 'type' => 'DelegateInterface']);

    $variables = [];

    $body = '';
    if ($structure['fields']['id']['type'] == 'uuid') {
        $variables[] = '    $' . lcfirst($structure['class']) . 'Identifier';
        $body .= '$' . lcfirst($structure['class']) . 'Identifier = $this->' . lcfirst($structure['class']) . 'Repository->nextIdentifier();';
    }

    $text = <<< EOT
try {
    \$payload = \$this->getPayloadFromRequest(\$request);
} catch (\Exception \$error) {
    return new JsonResponse([
        'error' => \$error->getMessage()
    ], StatusCodeInterface::STATUS_BAD_REQUEST);
}
EOT;

    $body .= PHP_EOL . PHP_EOL . $text;

    foreach ($structure['fields'] as $parameter => $field) {
        if ($parameter == 'id' && $field['type'] == 'uuid') {
            continue;
        } else if ($parameter == 'id' && $field['type'] == 'int') {
            $variables[] = strtolower('    $payload[\'' . camelCaseToUnderscore($structure['class']) . '_id\']');
            continue;
        }

        $variables[] = strtolower('    $payload[\'' . camelCaseToUnderscore($structure['class']) . '_' . camelCaseToUnderscore($parameter) . '\']');
    }

    $body .= PHP_EOL . PHP_EOL . '$create' . $structure['class'] . 'Command = Create' . $structure['class'] . 'Command::withData(' . PHP_EOL . implode(',       ' . PHP_EOL, $variables) . PHP_EOL . ');';

    $_class = lcfirst($structure['class']);

    $text = <<< EOT
try {
    \$this->{$_class}CommandHandler->__invoke(\$create{$structure['class']}Command);
} catch (\Exception \$error) {
    return new JsonResponse([
        'error' => \$error->getMessage()
    ], StatusCodeInterface::STATUS_BAD_REQUEST);
}
EOT;

    $body .= PHP_EOL . PHP_EOL . $text;
    $_structure = lcfirst($structure['class']);

    if ($structure['fields']['id']['type'] == 'uuid') {
        $text = <<< EOT
return new JsonResponse([
    'id' => (string) \${$_structure}Identifier,
], StatusCodeInterface::STATUS_CREATED);
EOT;
    } else {
        $_name = camelCaseToUnderscore($structure['class']);

        $text = <<< EOT
return new JsonResponse([
    'id' => \$payload['{$_name}_id'],
], StatusCodeInterface::STATUS_CREATED);
EOT;
    }

    $body .= PHP_EOL . PHP_EOL . $text;

    $method->setBody($body);
    $class->addMethodFromGenerator($method);

    /* Method "getPayloadFromRequest" */

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ParamTag('request', ['ServerRequestInterface']));
    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ReturnTag('array'));

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setDocBlock($docBlock);
    $method->setVisibility(\Zend\Code\Generator\PropertyGenerator::VISIBILITY_PRIVATE);
    $method->setName(implode('', ['getPayloadFromRequest']));
    $method->setParameter(['name' => 'request', 'type' => 'ServerRequestInterface']);
    $method->setReturnType('array');

    $body = '$payload = [];' . PHP_EOL . PHP_EOL . '$data = $request->getParsedBody();' . PHP_EOL . PHP_EOL;

    $parts = [];
    foreach ($structure['fields'] as $parameter => $field) {
        if ($parameter == 'id' && $field['type'] == 'uuid') {
            continue;
        } else if ($parameter == 'id' && $field['type'] == 'int') {
                $parts[] = strtolower('$payload[\'' . camelCaseToUnderscore($structure['class']) . '_' . 'id' . '\']' . ' = $data[\'' . 'id' . '\'] ?? null;');
                continue;
        }

        $parts[] = strtolower('$payload[\'' . camelCaseToUnderscore($structure['class']) . '_' . camelCaseToUnderscore($parameter) . '\']' . ' = $data[\'' . camelCaseToUnderscore($parameter) . '\'] ?? null;');
    }

    $body .= implode(PHP_EOL, $parts);
    $body .= PHP_EOL . PHP_EOL . 'return $payload;';

    $method->setBody($body);

    $class->addMethodFromGenerator($method);

    $file = new \Zend\Code\Generator\FileGenerator();
    $file->setBody('declare(strict_types = 1);' . PHP_EOL . PHP_EOL . $class->generate());

    $path = implode(DIRECTORY_SEPARATOR, [DIR_SRC, 'Api', 'Action', $structure['class']]);

    @mkdir($path, 0777, true);

    file_put_contents($path . DIRECTORY_SEPARATOR . $class->getName() . PHP, $file->generate());
}