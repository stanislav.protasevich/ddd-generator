<?php

foreach ($structures as $structure)
{
    global $mapping;
    global $constructParams;

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setShortDescription(implode(' ', ['Interface', 'Create' . $structure['class'] . 'CommandInterface']));
    $docBlock->setTag(['name' => 'package', 'description' => implode('\\', [BASE_NAMESPACE, 'Domain', 'Model', $structure['class'], 'Command'])]);

    $interface = new \Zend\Code\Generator\InterfaceGenerator();
    $interface->setNamespaceName(implode('\\', [BASE_NAMESPACE, 'Domain', 'Model', $structure['class'], 'Command']));
    $interface->setDocBlock($docBlock);
    $interface->setName('Create' . $structure['class'] . 'CommandInterface');
    $interface->addUse('ProDevZone\Common\Identifier\IdentifierInterface');

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $parts = [];
    foreach ($structure['fields'] as $parameter => $field) {
        if ($parameter == 'id') {
            $parameter = 'Identifier';
        }

        $name = lcfirst($structure['class']) . ucfirst($parameter);
        $type = 'string';

        if (isset($field['type']) && $field['type'] !== null && $field['type'] == 'uuid') {
            $type = 'IdentifierInterface';
        }

        if (isset($field['reference']) && $field['reference']['table'] !== null) {
            //$interface->addUse($field['namespace'] . INTERFACE_NAME_KEY);

            $name = $mapping[$field['reference']['table']] . 'Identifier';
            $type = 'string';
        }

        $types = [$type];

        if ($field['nullable']) {
            $types[] = 'null';
        }

        $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ParamTag(lcfirst(underscoreToCamel($name)), $types));
    }
    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ReturnTag('Create' . $structure['class'] . 'CommandInterface'));

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setName('withData');
    $method->setDocBlock($docBlock);
    $method->setStatic(true);
    $method->setReturnType('Create' . $structure['class'] . 'CommandInterface');
    foreach ($structure['fields'] as $parameter => $field) {
        if ($parameter == 'id') {
            $parameter = 'Identifier';
        }

        $name = lcfirst(underscoreToCamel(lcfirst($structure['class']) . ucfirst($parameter)));
        $type = 'string';

        if (isset($field['type']) && $field['type'] !== null && $field['type'] == 'uuid') {
            $type = 'IdentifierInterface';
        }

        if (isset($field['reference']) && $field['reference']['table'] !== null) {
            //$interface->addUse($field['namespace'] . INTERFACE_NAME_KEY);

            $name = $mapping[$field['reference']['table']] . 'Identifier';
            $type = 'string';
        }

        $parameter = new \Zend\Code\Generator\ParameterGenerator();
        $parameter->setName(lcfirst(underscoreToCamel($name)));
        $parameter->setType($type);

        if ($field['nullable']) {
            $parameter->setDefaultValue(null);
        }

        $method->setParameter($parameter);
    }
    $interface->addMethodFromGenerator($method);

    foreach ($structure['fields'] as $parameter => $field) {
        $docBlock = new \Zend\Code\Generator\DocBlockGenerator();

        $type = $field['type'];

        if ($type == 'DateTime') {
            $interface->addUse('\DateTime');
        }

        if ($parameter == 'id') {
            $parameter = 'Identifier';
            $type = 'IdentifierInterface';
        }

        $name = lcfirst($structure['class']) . ucfirst(underscoreToCamel($parameter));

        if (isset($field['reference']) && $field['reference']['table'] !== null) {
            //$interface->addUse($field['namespace'] . INTERFACE_NAME_KEY);


            $name = $mapping[$field['reference']['table']] . 'Identifier';
            $type = 'IdentifierInterface';
        }

        $_type = $type;

        if ($field['nullable'] == true) {
            $_type = $type . '|null';
        }

        $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ReturnTag($_type));

        $method = new \Zend\Code\Generator\MethodGenerator();
        $method->setDocBlock($docBlock);
        $method->setName($name);
        $method->setReturnType($type);

        if ($field['nullable'] == true) {
            $method->setReturnType('?' . $type);
        }

        $interface->addMethodFromGenerator($method);
    }



    $file = new \Zend\Code\Generator\FileGenerator();
    $file->setBody('declare(strict_types = 1);' . PHP_EOL . PHP_EOL . $interface->generate());

    $path = implode(DIRECTORY_SEPARATOR, [DIR_SRC, 'Domain', 'Model', $structure['class'], 'Command']);

    @mkdir($path, 0777, true);

    file_put_contents($path . DIRECTORY_SEPARATOR . $interface->getName() . PHP, $file->generate());
}