<?php

$factories = [];

foreach ($structures as $structure)
{
    $factories[] = '/* ' . $structure['class'] . ' */' . PHP_EOL;

    $command = '$app->post(' . PHP_EOL . "    '/api/v1/" . camelCaseToDash($structure['class']) . "s',"
        . PHP_EOL . '    ' . BASE_NAMESPACE . '\\Api\\Action\\' . $structure['class'] . '\\Create' . $structure['class'] . 'Action::class,' . PHP_EOL . "    'api.v1." . camelCaseToDash($structure['class']) . "s.create'" . PHP_EOL . ');';

    $factories[] = $command . PHP_EOL;

    $command = '$app->patch(' . PHP_EOL . "    '/api/v1/" . camelCaseToDash($structure['class']) . "s/:id',"
        . PHP_EOL . '    ' . BASE_NAMESPACE . '\\Api\\Action\\' . $structure['class'] . '\\Update' . $structure['class'] . 'Action::class,' . PHP_EOL . "    'api.v1." . camelCaseToDash($structure['class']) . "s.update'" . PHP_EOL . ');';

    $factories[] = $command . PHP_EOL;

    $command = '$app->delete(' . PHP_EOL . "    '/api/v1/" . camelCaseToDash($structure['class']) . "s/:id',"
        . PHP_EOL . '    ' . BASE_NAMESPACE . '\\Api\\Action\\' . $structure['class'] . '\\Delete' . $structure['class'] . 'Action::class,' . PHP_EOL . "    'api.v1." . camelCaseToDash($structure['class']) . "s.delete'" . PHP_EOL . ');';

    $factories[] = $command . PHP_EOL . PHP_EOL;
}


$file = new \Zend\Code\Generator\FileGenerator();
$file->setBody('<?php' . PHP_EOL . PHP_EOL . implode(PHP_EOL, $factories));

$path = implode(DIRECTORY_SEPARATOR, [DIR_SRC]);

@mkdir($path, 0777, true);

file_put_contents($path . DIRECTORY_SEPARATOR . 'routes' . PHP, $file->generate());