<?php

/** @var array $structure */

foreach ($structures as $structure) {
    global $mapping;

    $primaryKey = $structure['fields']['id'];

    $constructParams = [];

    // Model Interface
    $dockBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $dockBlock->setShortDescription(implode(' ', [ucfirst(INTERFACE_NAME_KEY), $structure[CLASS_NAME_KEY] . ucfirst(INTERFACE_NAME_KEY)]));
    $dockBlock->setTag(['name' => 'package', 'description' => $structure[NAMESPACE_NAME_KEY]]);

    $interface = new \Zend\Code\Generator\InterfaceGenerator();
    $interface->setNamespaceName($structure[NAMESPACE_NAME_KEY]);
    $interface->setDocBlock($dockBlock);
    $interface->setName($structure[CLASS_NAME_KEY] . ucfirst(INTERFACE_NAME_KEY));
    $interface->addUse('ProDevZone\Common\Identifier\IdentifierInterface');

    // Identifier getter
    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setShortDescription(implode(' ', ['Get', lcfirst($structure[CLASS_NAME_KEY]), 'identifier']));
    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ReturnTag('IdentifierInterface'));

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setDocBlock($docBlock);
    $method->setName(implode('', ['get', 'Identifier']));
    $method->setReturnType('IdentifierInterface');
    $interface->addMethodFromGenerator($method);

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setShortDescription(implode(' ', ['Set', lcfirst($structure[CLASS_NAME_KEY]), 'identifier']));
    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ParamTag('identifier', ['IdentifierInterface']));
    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ReturnTag($structure[CLASS_NAME_KEY] . ucfirst(INTERFACE_NAME_KEY)));

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setDocBlock($docBlock);
    $method->setName(implode('', ['set', 'Identifier']));
    $method->setParameter(['type' => 'IdentifierInterface', 'name' => 'identifier']);
    $method->setReturnType($structure[CLASS_NAME_KEY] . ucfirst(INTERFACE_NAME_KEY));
    $interface->addMethodFromGenerator($method);

    $params = ['types' => ['IdentifierInterface'], 'name' => 'identifier', 'nullable' => false];
    if ($params['nullable']) {
        $params['types'][] = 'null';
    }
    $constructParams[] = $params;

    foreach ($structure['fields'] as $name => $field) {
        if ($name == 'id') continue;

        $type = null;
        if (isset($field['type'])) {
            $type = $field['type'];
        }

        if (isset($field['reference']) && $field['reference']['table'] !== null) {
            $name = $mapping[$field['reference']['table']];
            $type = ucfirst($name) . ucfirst(INTERFACE_NAME_KEY);
        }

        if ($type == 'DateTime') {
            $interface->addUse('DateTime');
        }

        // Getter
        $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
        $docBlock->setShortDescription(implode(' ', ['Get', camelCaseToSeparator(underscoreToCamel($name))]));
        $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ReturnTag([$type]));

        $method = new \Zend\Code\Generator\MethodGenerator();
        $method->setDocBlock($docBlock);
        $method->setName(implode('', ['get', ucfirst(underscoreToCamel($name))]));
        $method->setReturnType(implode('', [$type]));
        $interface->addMethodFromGenerator($method);

        // Setter
        $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
        $docBlock->setShortDescription(implode(' ', ['Set', camelCaseToSeparator(underscoreToCamel($name))]));

        $types = [$type];
        if ($field['nullable']) {
            $types[] = 'null';
        }

        $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ParamTag(lcfirst(underscoreToCamel($name)), $types));
        $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ReturnTag([$structure[CLASS_NAME_KEY] . ucfirst(INTERFACE_NAME_KEY)]));

        $method = new \Zend\Code\Generator\MethodGenerator();
        $method->setDocBlock($docBlock);
        $method->setName(implode('', ['set', ucfirst(underscoreToCamel($name))]));

        $parameterSettings = ['type' => $type, 'name' => lcfirst(underscoreToCamel($name))];
        if (isset($field[DEFAULT_NAME_KEY])) {
            $parameterSettings['defaultValue'] = $field[DEFAULT_NAME_KEY];
        } else if ($field['nullable']) {
            $parameterSettings['defaultValue'] = null;
        }

        $method->setParameter($parameterSettings);
        $method->setReturnType($structure[CLASS_NAME_KEY] . ucfirst(INTERFACE_NAME_KEY));
        $interface->addMethodFromGenerator($method);

        $params = ['types' => [$type], 'name' => $name, NULLABLE_NAME_KEY => $field[NULLABLE_NAME_KEY]];
        if ($params['nullable']) {
            $params['types'][] = 'null';
        }

        if (isset($parameterSettings['defaultValue'])) {
            $params[DEFAULT_NAME_KEY] = $parameterSettings['defaultValue'];
        }

        $constructParams[] = $params;
    }

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setShortDescription(implode(' ', ['Create', camelCaseToSeparator($structure[CLASS_NAME_KEY])]));
    foreach ($constructParams as $constructParam) {
        $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ParamTag(lcfirst(underscoreToCamel($constructParam['name'])), $constructParam['types']));
    }
    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ReturnTag([$structure[CLASS_NAME_KEY] . ucfirst(INTERFACE_NAME_KEY)]));

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setStatic(true);
    $method->setDocBlock($docBlock);
    $method->setName(implode('', ['withData']));
    foreach ($constructParams as $constructParam) {
        $parameter = new \Zend\Code\Generator\ParameterGenerator();
        $parameter->setName(lcfirst(underscoreToCamel($constructParam['name'])));
        $parameter->setType($constructParam['types'][0]);

        if (isset($constructParam[DEFAULT_NAME_KEY])) {
            $parameter->setDefaultValue($constructParam[DEFAULT_NAME_KEY]);
        } else if (in_array('null', $constructParam['types'])) {
            $parameter->setDefaultValue(null);
        }

        $method->setParameter($parameter);
    }

    $method->setReturnType($structure[CLASS_NAME_KEY] . ucfirst(INTERFACE_NAME_KEY));
    $interface->addMethodFromGenerator($method);

    $file = new \Zend\Code\Generator\FileGenerator();
    $file->setBody('declare(strict_types = 1);' . PHP_EOL . PHP_EOL . $interface->generate());

    $path = implode(DIRECTORY_SEPARATOR, [DIR_SRC, $structure[CONTEXT_NAME_KEY], DOMAIN_NAME_KEY, MODEL_NAME_KEY]);

    @mkdir($path, 0777, true);
    file_put_contents($path . DIRECTORY_SEPARATOR . $structure[CLASS_NAME_KEY] . ucfirst(INTERFACE_NAME_KEY) . PHP, $file->generate());

    /* Entity */

    $dockBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $dockBlock->setShortDescription(implode(' ', [ucfirst(CLASS_NAME_KEY), $structure[CLASS_NAME_KEY]]));
    $dockBlock->setTag(['name' => 'package', 'description' => $structure[NAMESPACE_NAME_KEY]]);

    $class = new \Zend\Code\Generator\ClassGenerator();
    $class->setFinal(true);
    $class->setNamespaceName($structure[NAMESPACE_NAME_KEY]);
    $class->setDocBlock($dockBlock);
    $class->setName($structure[CLASS_NAME_KEY]);
    $class->setImplementedInterfaces([$structure[NAMESPACE_NAME_KEY] . '\\' . $structure[CLASS_NAME_KEY] . ucfirst(INTERFACE_NAME_KEY)]);
    $class->setExtendedClass('ProDevZone\Common\Identifier\IdentifiableEntity');
    $class->addUse('ProDevZone\Common\Identifier\IdentifierInterface');
    $class->addUse('ProDevZone\Common\Identifier\IdentifiableEntity');

    // Constructor
    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setShortDescription(implode(' ', [$structure[CLASS_NAME_KEY], 'constructor.']));
    foreach ($constructParams as $constructParam) {
        $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ParamTag(lcfirst(underscoreToCamel($constructParam['name'])), $constructParam['types']));
    }

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setDocBlock($docBlock);
    $method->setVisibility(\Zend\Code\Generator\PropertyGenerator::VISIBILITY_PRIVATE);
    $method->setName(implode('', ['__construct']));
    $parts = [];
    foreach ($constructParams as $constructParam) {
        $parameter = new \Zend\Code\Generator\ParameterGenerator();
        $parameter->setName(lcfirst(underscoreToCamel($constructParam['name'])));
        $parameter->setType($constructParam['types'][0]);

        if (isset($constructParam[DEFAULT_NAME_KEY])) {
            $parameter->setDefaultValue($constructParam[DEFAULT_NAME_KEY]);
        } else if (in_array('null', $constructParam['types'])) {
            $parameter->setDefaultValue(null);
        }

        if (in_array('DateTime', $constructParam['types'])) {
            $class->addUse('\DateTime');
        }

        $method->setParameter($parameter);

        $parts[] = '$this->set' . ucfirst(underscoreToCamel($constructParam['name'])) . '($' . lcfirst(underscoreToCamel($constructParam['name'])) . ');';
    }
    $method->setBody(implode(PHP_EOL, $parts));
    $class->addMethodFromGenerator($method);

    // Static withData
    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\GenericTag('@inheritdoc'));

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setStatic(true);
    $method->setDocBlock($docBlock);
    $method->setName(implode('', ['withData']));
    $parts = [];
    foreach ($constructParams as $constructParam) {
        $parameter = new \Zend\Code\Generator\ParameterGenerator();
        $parameter->setName(lcfirst(underscoreToCamel($constructParam['name'])));
        $parameter->setType($constructParam['types'][0]);

        if (isset($constructParam[DEFAULT_NAME_KEY])) {
            $parameter->setDefaultValue($constructParam[DEFAULT_NAME_KEY]);
        } else if (in_array('null', $constructParam['types'])) {
            $parameter->setDefaultValue(null);
        }

        $method->setParameter($parameter);

        $parts[] = '$' . lcfirst(underscoreToCamel($constructParam['name']));
    }

    $string = implode(', ', $parts);
    $method->setReturnType($structure[CLASS_NAME_KEY] . ucfirst(INTERFACE_NAME_KEY));
    $body = <<< EOT
return new self($string);
EOT;
    $method->setBody($body);
    $class->addMethodFromGenerator($method);

    //Properties
    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setTag(
        (new \Zend\Code\Generator\DocBlock\Tag\VarTag(
            'identifier',
            ['IdentifierInterface']
        ))
    );

    $property = new \Zend\Code\Generator\PropertyGenerator();
    $property->setDocBlock($docBlock);
    $property->setName('identifier');
    $property->setVisibility(\Zend\Code\Generator\PropertyGenerator::VISIBILITY_PRIVATE);
    $class->addPropertyFromGenerator($property);

    /* Identifier getter */
    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\GenericTag('@inheritdoc'));

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setDocBlock($docBlock);
    $method->setName(implode('', ['get', 'Identifier']));
    $method->setReturnType('IdentifierInterface');
    $body = <<< EOT
if (\$this->identifier === null) {
    \$this->identifier = \$this->id;
}

return \$this->identifier;
EOT;
    $method->setBody($body);
    $class->addMethodFromGenerator($method);

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\GenericTag('@inheritdoc'));

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setDocBlock($docBlock);
    $method->setName(implode('', ['set', 'Identifier']));
    $method->setParameter(['type' => 'IdentifierInterface', 'name' => 'identifier']);
    $method->setReturnType($structure[CLASS_NAME_KEY] . ucfirst(INTERFACE_NAME_KEY));
    $body = <<< EOT
\$this->setId(\$identifier);

\$this->identifier = \$identifier;

return \$this;
EOT;
    $method->setBody($body);
    $class->addMethodFromGenerator($method);

    foreach ($structure['fields'] as $name => $field) {
        if ($name == 'id') continue;

        $type = null;
        if (isset($field['type'])) {
            $type = $field['type'];
        }

        if (isset($field['reference']) && $field['reference']['table'] !== null) {
            $name = $mapping[$field['reference']['table']];
            $type = ucfirst($name) . ucfirst(INTERFACE_NAME_KEY);
        }

        $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
        $docBlock->setTag(
            (new \Zend\Code\Generator\DocBlock\Tag\VarTag(
                $name,
                [$type]
            ))
        );

        $property = new \Zend\Code\Generator\PropertyGenerator();
        $property->setDocBlock($docBlock);
        $property->setName(lcfirst(underscoreToCamel($name)));
        $property->setVisibility(\Zend\Code\Generator\PropertyGenerator::VISIBILITY_PRIVATE);
        $class->addPropertyFromGenerator($property);

        /* Getter */
        $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
        $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\GenericTag('@inheritdoc'));

        $method = new \Zend\Code\Generator\MethodGenerator();
        $method->setDocBlock($docBlock);
        $method->setName(implode('', ['get', underscoreToCamel($name)]));
        $method->setReturnType(implode('', [$type]));
        $_name = lcfirst(underscoreToCamel($name));
        $body = <<< EOT
return \$this->{$_name};
EOT;
        $method->setBody($body);
        $class->addMethodFromGenerator($method);

        /* Setter */
        $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
        $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\GenericTag('@inheritdoc'));

        $method = new \Zend\Code\Generator\MethodGenerator();
        $method->setDocBlock($docBlock);
        $method->setName(implode('', ['set', underscoreToCamel($name)]));

        $parameterSettings = ['type' => $type, 'name' => lcfirst(underscoreToCamel($name))];

        if (isset($field[DEFAULT_NAME_KEY])) {
            $parameterSettings['defaultValue'] = $field[DEFAULT_NAME_KEY];
        } else if ($field['nullable']) {
            $parameterSettings['defaultValue'] = null;
        }
        $method->setParameter($parameterSettings);

        $method->setReturnType($structure[CLASS_NAME_KEY] . ucfirst(INTERFACE_NAME_KEY));
        $body = <<< EOT
\$this->$_name = \$$_name;

return \$this;
EOT;
        $method->setBody($body);
        $class->addMethodFromGenerator($method);
    }

    $file = new \Zend\Code\Generator\FileGenerator();
    $file->setBody('declare(strict_types = 1);' . PHP_EOL . PHP_EOL . $class->generate());

    $path = implode(DIRECTORY_SEPARATOR, [DIR_SRC, $structure[CONTEXT_NAME_KEY], DOMAIN_NAME_KEY, MODEL_NAME_KEY]);

    @mkdir($path, 0777, true);
    file_put_contents($path . DIRECTORY_SEPARATOR . $structure[CLASS_NAME_KEY] . PHP, $file->generate());

    $path = implode(DIRECTORY_SEPARATOR, [DIR_SRC, $structure[CONTEXT_NAME_KEY], DOMAIN_NAME_KEY, MODEL_NAME_KEY, $structure['class']]);
    @mkdir($path, 0777, true);
}