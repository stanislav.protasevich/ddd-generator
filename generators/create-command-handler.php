<?php

foreach ($structures as $structure)
{
    global $mapping;
    global $constructParams;

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setShortDescription(implode(' ', ['Class', 'Create' . $structure['class'] . 'CommandHandler']));
    $docBlock->setTag(['name' => 'package', 'description' => implode('\\', [BASE_NAMESPACE, 'Domain', 'Model', $structure['class'], 'Command', 'Handler'])]);

    $class = new \Zend\Code\Generator\ClassGenerator();
    $class->setFinal(true);
    $class->setNamespaceName(implode('\\', [BASE_NAMESPACE, 'Domain', 'Model', $structure['class'], 'Command', 'Handler']));
    $class->setDocBlock($docBlock);
    $class->setName('Create' . $structure['class'] . 'CommandHandler');
    $class->addUse(implode('\\', [BASE_NAMESPACE, 'Domain', 'Model', $structure['class'], 'Command', 'Create' . $structure['class'] . 'CommandInterface']));

    $variables = [];
    $parts = [];
    foreach ($structure['fields'] as $parameter => $field) {
        if ($parameter == 'id') {
            $name = lcfirst($structure['class']) . 'Repository';
            $type = ucfirst($name) . 'Interface';

            $class->addUse(implode('\\', [BASE_NAMESPACE, 'Domain', 'Model', $structure['class'], $structure['class'] . 'RepositoryInterface']));
        } else if (isset($field['reference']) && $field['reference']['table'] !== null) {
            $name = $mapping[$field['reference']['table']];
            $type = ucfirst($name) . 'Interface';

            $class->addUse(implode('\\', [BASE_NAMESPACE, 'Domain', 'Model', ucfirst($name), ucfirst($name) . 'RepositoryInterface']));

            $name = $mapping[$field['reference']['table']] . 'Repository';
            $type = ucfirst($name) . 'Interface';
        } else {
            continue;
        }

        $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
        $docBlock->setTag(
            (new \Zend\Code\Generator\DocBlock\Tag\VarTag(
                $name,
                [$type]
            ))
        );

        $property = new \Zend\Code\Generator\PropertyGenerator();
        $property->setDocBlock($docBlock);
        $property->setName($name);
        $property->setVisibility(\Zend\Code\Generator\PropertyGenerator::VISIBILITY_PRIVATE);
        $class->addPropertyFromGenerator($property);
    }

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setShortDescription(implode(' ', ['Create' . $structure['class'] . 'CommandHandler', 'constructor.']));

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setName(implode('', ['__construct']));
    $parts = [];
    foreach ($structure['fields'] as $parameter => $field) {
        if ($parameter == 'id') {
            $name = lcfirst($structure['class']) . 'Repository';
            $type = ucfirst($name) . 'Interface';

            $class->addUse(implode('\\', [BASE_NAMESPACE, 'Domain', 'Model', $structure['class'], $structure['class'] . 'RepositoryInterface']));
        } else if (isset($field['reference']) && $field['reference']['table'] !== null) {
            $name = $mapping[$field['reference']['table']];
            $type = ucfirst($name) . 'Interface';

            $class->addUse(implode('\\', [BASE_NAMESPACE, 'Domain', 'Model', ucfirst($name), ucfirst($name) . 'RepositoryInterface']));

            $name = $mapping[$field['reference']['table']] . 'Repository';
            $type = ucfirst($name) . 'Interface';
        } else {
            continue;
        }

        $parameter = new \Zend\Code\Generator\ParameterGenerator();
        $parameter->setName($name);
        $parameter->setType($type);

        $method->setParameter($parameter);

        $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ParamTag($name, [$type]));

        $parts[] = '$this->' . $name . ' = $' . $name. ';';
    }
    $method->setDocBlock($docBlock);
    $method->setBody(implode(PHP_EOL, $parts));
    $class->addMethodFromGenerator($method);

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ParamTag('command', [implode(' ', ['Create' . $structure['class'] . 'CommandInterface'])]));

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setName('__invoke');
    $method->setDocBlock($docBlock);

    foreach ($structure['fields'] as $parameter => $field) {
        if ($parameter != 'id') {
            continue;
        }

        $parameter = 'Identifier';

        $name = 'command';
        $type = 'Create' . $structure['class'] . 'CommandInterface';

//        if (isset($field['type']) && $field['type'] !== null && $field['type'] == 'uuid') {
//            $type = 'IdentifierInterface';
//        }

        $parameter = new \Zend\Code\Generator\ParameterGenerator();
        $parameter->setName($name);
        $parameter->setType($type);

        $method->setParameter($parameter);
    }

    $body = '';

    $parts = [];
    foreach ($structure['fields'] as $parameter => $field) {
        if ($parameter == 'id') {
            $name = lcfirst(underscoreToCamel($structure['class']));
            $type = ucfirst($name) . 'Interface';
        } else {
            continue;
        }

        $variables[] = '$' . $name . 'Identifier';

        $parts[] =  <<< EOT
\${$name}Identifier = \$command->{$name}Identifier();
EOT;
    }

    $body .= implode(PHP_EOL . PHP_EOL, $parts);

    $parts = [];
    foreach ($structure['fields'] as $parameter => $field) {
        if ($parameter == 'id') {
            continue;
        } else if (isset($field['reference']) && $field['reference']['table'] !== null) {
            $name = $mapping[$field['reference']['table']];
            $type = ucfirst($name) . 'Interface';

            $class->addUse(implode('\\', [BASE_NAMESPACE, 'Domain', 'Model', ucfirst($name), ucfirst($name) . 'RepositoryInterface']));

            $name = $mapping[$field['reference']['table']] . 'Repository';
            $type = ucfirst($name) . 'Interface';
        } else {
            continue;
        }

        $_repository = $name;

        $variables[] = '$' . $mapping[$field['reference']['table']];

        if ($field['nullable']) {
            $parts[] =  <<< EOT
\${$mapping[$field['reference']['table']]} = \$command->{$mapping[$field['reference']['table']]}Identifier();
if (\${$mapping[$field['reference']['table']]} !== null) {
    \${$mapping[$field['reference']['table']]} = \$this->{$_repository}->findOneOrFail(
        [
            'id' => \${$mapping[$field['reference']['table']]}
        ]
    );
}

EOT;
        } else {
            $parts[] =  <<< EOT
\${$mapping[$field['reference']['table']]} = \$this->{$_repository}->findOneOrFail(
    [
        'id' => \$command->{$mapping[$field['reference']['table']]}Identifier()
    ]
);

EOT;
        }
    }

    if (count($parts) > 0) {
        $body .= PHP_EOL . PHP_EOL . implode(PHP_EOL, $parts);
    }

    $parts = [];
    foreach ($structure['fields'] as $parameter => $field) {
        if ($parameter == 'id') {
            continue;
        } else if (isset($field['reference']) && $field['reference']['table'] !== null) {
            continue;
        }

        $_name = lcfirst($structure['class']) . ucfirst(underscoreToCamel($parameter));

        $variables[] = '$' . $_name;

        $parts[] =  <<< EOT
\${$_name} = \$command->{$_name}();
EOT;
    }

    $body .= PHP_EOL . PHP_EOL . implode(PHP_EOL . PHP_EOL, $parts);

    $_class = ucfirst($structure['class']);
    $__class = lcfirst($structure['class']);
    $variables = implode(', ', $variables);

    $class->addUse(implode('\\', [BASE_NAMESPACE, 'Domain', 'Model', ucfirst($structure['class'])]));

    $text = <<< EOT
\${$__class} = {$_class}::withData({$variables});
EOT;

    $body .= PHP_EOL . PHP_EOL . $text;

    $text = '$this->' . $__class . 'Repository->create($' . $__class . ');';

    $body .= PHP_EOL . PHP_EOL . $text;

    $method->setBody($body);
    $class->addMethodFromGenerator($method);

    $file = new \Zend\Code\Generator\FileGenerator();
    $file->setBody('declare(strict_types = 1);' . PHP_EOL . PHP_EOL . $class->generate());

    $path = implode(DIRECTORY_SEPARATOR, [DIR_SRC, 'Domain', 'Model', $structure['class'], 'Command', 'Handler']);

    @mkdir($path, 0777, true);

    file_put_contents($path . DIRECTORY_SEPARATOR . $class->getName() . PHP, $file->generate());
}