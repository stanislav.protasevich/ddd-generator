<?php

foreach ($structures as $structure)
{
    global $mapping;
    global $constructParams;

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setShortDescription(implode(' ', ['Class', 'Create' . $structure['class'] . 'Command']));
    $docBlock->setTag(['name' => 'package', 'description' => implode('\\', [BASE_NAMESPACE, 'Domain', 'Model', $structure['class'], 'Command'])]);

    $class = new \Zend\Code\Generator\ClassGenerator();
    $class->setFinal(true);
    $class->setNamespaceName(implode('\\', [BASE_NAMESPACE, 'Domain', 'Model', $structure['class'], 'Command']));
    $class->setImplementedInterfaces([implode('\\', [BASE_NAMESPACE, 'Domain', 'Model', $structure['class'], 'Command', 'Create' . $structure['class'] . 'CommandInterface'])]);
    $class->setDocBlock($docBlock);
    $class->setName('Create' . $structure['class'] . 'Command');
    $class->addUse('ProDevZone\Common\Identifier\IdentifierInterface');

    $parts = [];
    foreach ($structure['fields'] as $parameter => $field) {
        if ($parameter == 'id') {
            $parameter = 'Identifier';
        }

        $name = lcfirst($structure['class']) . ucfirst(underscoreToCamel($parameter));
        $type = 'string';

        if (isset($field['reference']) && $field['reference']['table'] !== null) {
            //$interface->addUse($field['namespace'] . INTERFACE_NAME_KEY);

            $name = $mapping[$field['reference']['table']] . 'Identifier';
            $type = 'string';
        }

        $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
        $docBlock->setTag(
            (new \Zend\Code\Generator\DocBlock\Tag\VarTag(
                $name,
                [$type]
            ))
        );

        $property = new \Zend\Code\Generator\PropertyGenerator();
        $property->setDocBlock($docBlock);
        $property->setName($name);
        $property->setVisibility(\Zend\Code\Generator\PropertyGenerator::VISIBILITY_PRIVATE);
        $class->addPropertyFromGenerator($property);
    }

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setShortDescription(implode(' ', ['Create' . $structure['class'] . 'Command', 'constructor.']));
    foreach ($constructParams as $constructParam) {

    }

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setVisibility(\Zend\Code\Generator\PropertyGenerator::VISIBILITY_PRIVATE);
    $method->setName(implode('', ['__construct']));
    $parts = [];
    foreach ($structure['fields'] as $parameter => $field) {
        if ($parameter == 'id') {
            $parameter = 'Identifier';
        }

        $name = lcfirst($structure['class']) . ucfirst(underscoreToCamel($parameter));
        $type = 'string';

        if (isset($field['type']) && $field['type'] !== null && $field['type'] == 'uuid') {
            $type = 'IdentifierInterface';
        }

        if (isset($field['reference']) && $field['reference']['table'] !== null) {
            //$interface->addUse($field['namespace'] . INTERFACE_NAME_KEY);

            $name = $mapping[$field['reference']['table']] . 'Identifier';
            $type = 'string';
        }

        $parameter = new \Zend\Code\Generator\ParameterGenerator();
        $parameter->setName($name);
        $parameter->setType($type);

        if ($field['nullable']) {
            $parameter->setDefaultValue(null);
        }

        $method->setParameter($parameter);

        $types = [$type];
        if ($field['nullable']) {
            $types[] = 'null';
        }

        $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ParamTag($name, $types));

        $parts[] = '$this->' . $name . ' = $' . $name. ';';
    }
    $method->setDocBlock($docBlock);
    $method->setBody(implode(PHP_EOL, $parts));
    $class->addMethodFromGenerator($method);

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\GenericTag('@inheritdoc'));

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setName('withData');
    $method->setDocBlock($docBlock);
    $method->setStatic(true);
    $method->setReturnType('Create' . $structure['class'] . 'CommandInterface');
    foreach ($structure['fields'] as $parameter => $field) {
        if ($parameter == 'id') {
            $parameter = 'Identifier';
        }

        $name = lcfirst($structure['class']) . ucfirst(underscoreToCamel($parameter));
        $type = 'string';

        if (isset($field['type']) && $field['type'] !== null && $field['type'] == 'uuid') {
            $type = 'IdentifierInterface';
        }

        if (isset($field['reference']) && $field['reference']['table'] !== null) {
            //$interface->addUse($field['namespace'] . INTERFACE_NAME_KEY);

            $name = $mapping[$field['reference']['table']] . 'Identifier';
            $type = 'string';
        }

        $parameter = new \Zend\Code\Generator\ParameterGenerator();
        $parameter->setName($name);
        $parameter->setType($type);

        if ($field['nullable']) {
            $parameter->setDefaultValue(null);
        }

        $method->setParameter($parameter);
    }
    $parts = [];
    foreach ($structure['fields'] as $parameter => $field) {
        $docBlock = new \Zend\Code\Generator\DocBlockGenerator();

        $type = $field['type'];

        if ($parameter == 'id') {
            $parameter = 'Identifier';
            $type = 'IdentifierInterface';
        }

        $name = lcfirst($structure['class']) . ucfirst(underscoreToCamel($parameter));

        if (isset($field['reference']) && $field['reference']['table'] !== null) {
            //$interface->addUse($field['namespace'] . INTERFACE_NAME_KEY);


            $name = $mapping[$field['reference']['table']] . 'Identifier';
            $type = 'IdentifierInterface';
        }

        $parts[] = '$' . $name;
    }

    $string = implode(', ', $parts);
    $body = <<< EOT
return new self($string);
EOT;
    $method->setBody($body);
    $class->addMethodFromGenerator($method);

    foreach ($structure['fields'] as $parameter => $field) {
        $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
        $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\GenericTag('@inheritdoc'));

        $type = $field['type'];
        $_type = $field['type'];
        $_parameter = $parameter;
        if ($parameter == 'id') {
            $parameter = 'Identifier';

            $type = 'IdentifierInterface';
        }

        $name = lcfirst($structure['class']) . ucfirst(underscoreToCamel($parameter));
        $return = 'return (' . $_type . ') ' . '$this->' . $name . ';';

        if ($_parameter == 'id' && $field['type'] == 'int' && $field['nullable']) {
            $class->addUse('ProDevZone\Common\Identifier\IntegerIdentifier');
            $return = 'if ($this->' . $name . ' !== null) {' . PHP_EOL . '    return IntegerIdentifier::fromString($this->' . $name . ');' . PHP_EOL . '}' . PHP_EOL . PHP_EOL . 'return null;';
        } else if ($_parameter == 'id' && $field['type'] == 'sid' && $field['nullable']) {
            $class->addUse('ProDevZone\Common\Identifier\StringIdentifier');
            $return = 'if ($this->' . $name . ' !== null) {' . PHP_EOL . '    return StringIdentifier::fromString($this->' . $name . ');' . PHP_EOL . '}' . PHP_EOL . PHP_EOL . 'return null;';
        } else if ($_parameter == 'id' && $field['type'] == 'int') {
            $class->addUse('ProDevZone\Common\Identifier\IntegerIdentifier');
            $return = 'return IntegerIdentifier::fromString($this->' . $name . ');';
        } else if ($_parameter == 'id' && $field['type'] == 'sid') {
            $class->addUse('ProDevZone\Common\Identifier\StringIdentifier');
            $return = 'return StringIdentifier::fromString($this->' . $name . ');';
        } else if ($_parameter == 'id' && $field['type'] == 'uuid') {
            $return = 'return $this->' . $name . ';';
        } else if ($field['type'] == 'DateTime' && !$field['nullable']) {
            $class->addUse('DateTime');

            $return = 'return new DateTime($this->' . $name . ');';
        } else if ($field['type'] == 'DateTime' && $field['nullable']) {
            $class->addUse('DateTime');

            $return = 'if ($this->' . $name . ' !== null) {' . PHP_EOL . '    return new DateTime($this->' . $name . ');' . PHP_EOL . '}' . PHP_EOL . PHP_EOL . 'return null;';
        }

        if (isset($field['reference']) && $field['reference']['table'] !== null && $field['nullable']) {
            $name = $mapping[$field['reference']['table']] . 'Identifier';
            $type = 'IdentifierInterface';

            $class->addUse('ProDevZone\Common\Identifier\Uuid4Identifier');

            $return = 'if ($this->' . $name . ' !== null) {' . PHP_EOL . '    return Uuid4Identifier::fromString($this->' . $name . ');' . PHP_EOL . '}' . PHP_EOL . PHP_EOL . 'return null;';
        } else if (isset($field['reference']) && $field['reference']['table'] !== null) {
            $name = $mapping[$field['reference']['table']] . 'Identifier';
            $type = 'IdentifierInterface';

            $class->addUse('ProDevZone\Common\Identifier\Uuid4Identifier');

            $return = 'return Uuid4Identifier::fromString($this->' . $name . ');';
        }

        if ($field['nullable']) {
            $type = '?' . $type;
        }

        $method = new \Zend\Code\Generator\MethodGenerator();
        $method->setDocBlock($docBlock);
        $method->setName($name);
        $method->setReturnType($type);

        $body = <<< EOT
$return
EOT;
        $method->setBody($body);

        $class->addMethodFromGenerator($method);
    }

    $file = new \Zend\Code\Generator\FileGenerator();
    $file->setBody('declare(strict_types = 1);' . PHP_EOL . PHP_EOL . $class->generate());

    $path = implode(DIRECTORY_SEPARATOR, [DIR_SRC, 'Domain', 'Model', $structure['class'], 'Command']);

    @mkdir($path, 0777, true);

    file_put_contents($path . DIRECTORY_SEPARATOR . $class->getName() . PHP, $file->generate());
}