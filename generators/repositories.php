<?php

foreach ($structures as $structure)
{
    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setShortDescription(implode(' ', ['Class', 'Repository' . $structure['class']]));
    $docBlock->setTag(['name' => 'package', 'description' => implode('\\', [BASE_NAMESPACE, 'Infrastructure', 'Persistence', 'Repository'])]);

    $class = new \Zend\Code\Generator\ClassGenerator();
    $class->setNamespaceName(implode('\\', [BASE_NAMESPACE, 'Infrastructure', 'Persistence', 'Repository']));
    $class->setDocBlock($docBlock);
    $class->setFinal(true);
    $class->setImplementedInterfaces([implode('\\', [BASE_NAMESPACE, DOMAIN_NAME_KEY, MODEL_NAME_KEY, $structure['class'], $structure['class'] . 'Repository' . ucfirst(INTERFACE_NAME_KEY)])]);
    $class->addUse(implode('\\', [BASE_NAMESPACE, DOMAIN_NAME_KEY, MODEL_NAME_KEY, $structure['class'] . ucfirst(INTERFACE_NAME_KEY)]));
    $class->addUse(implode('\\', [BASE_NAMESPACE, DOMAIN_NAME_KEY, MODEL_NAME_KEY, $structure['class'], $structure['class'] . 'Mapper' . ucfirst(INTERFACE_NAME_KEY)]));
    $class->addUse(implode('\\', [BASE_NAMESPACE, DOMAIN_NAME_KEY, MODEL_NAME_KEY, $structure['class'], $structure['class'] . 'Repository' . ucfirst(INTERFACE_NAME_KEY)]));

    if ($structure['fields']['id']['type'] == 'uuid') {
        $class->addUse('ProDevZone\Common\Identifier\IdentifierInterface');
        $class->addUse('ProDevZone\Common\Identifier\Uuid4Identifier');
        $class->addUse('Ramsey\Uuid\Uuid');
    }

    $class->addUse('InvalidArgumentException');
    $class->setName($structure['class'] . 'Repository');

    //Properties
    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setTag(
        (new \Zend\Code\Generator\DocBlock\Tag\VarTag(
            'mapper',
            [$structure['class'] . 'Mapper' . ucfirst(INTERFACE_NAME_KEY)]
        ))
    );

    $property = new \Zend\Code\Generator\PropertyGenerator();
    $property->setDocBlock($docBlock);
    $property->setName('mapper');
    $property->setVisibility(\Zend\Code\Generator\PropertyGenerator::VISIBILITY_PRIVATE);
    $class->addPropertyFromGenerator($property);

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setShortDescription('Doctrine'. $structure['class'] . 'Repository constructor');
    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ParamTag('mapper', [$structure['class'] . 'MapperInterface']));

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setName('__construct');
    $method->setDocBlock($docBlock);
    $method->setParameter(['name' => 'mapper', 'type' => $structure['class'] . 'MapperInterface']);
    $_class = $structure['class'];
    $body = <<< EOT
\$this->mapper = \$mapper;
EOT;
    $method->setBody($body);
    $class->addMethodFromGenerator($method);

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\GenericTag('@inheritdoc'));

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setDocBlock($docBlock);
    $method->setName(implode('', ['findAllBy']));
    $method->setParameter(['name' => 'conditions', 'type' => 'array', 'defaultValue' => []]);
    $method->setReturnType('array');
    $body = <<< EOT
return \$this->mapper->fetchAllBy(\$conditions);
EOT;
    $method->setBody($body);
    $class->addMethodFromGenerator($method);

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setDocBlock($docBlock);
    $method->setName(implode('', ['findOneBy']));
    $method->setParameter(['name' => 'conditions', 'type' => 'array', 'defaultValue' => []]);
    $method->setReturnType('?'. $structure['class'] . 'Interface');
    $__class = lcfirst($_class);
    $_interface = $_class . 'Interface';
    $body = <<< EOT
return \$this->mapper->fetchOneBy(\$conditions);
EOT;
    $method->setBody($body);
    $class->addMethodFromGenerator($method);

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setName(implode('', ['findOneOrFail']));
    $method->setDocBlock($docBlock);
    $method->setParameter(['name' => 'conditions', 'type' => 'array', 'defaultValue' => []]);
    $method->setReturnType($structure['class'] . 'Interface');
    $body = <<< EOT
\$$__class = \$this->mapper->fetchOneBy(\$conditions);

if (!\$$__class) {
    throw new InvalidArgumentException('$_class cannot be found: "' . json_encode(\$conditions) . '".');
}

return \$$__class;
EOT;
    $method->setBody($body);
    $class->addMethodFromGenerator($method);

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setName(implode('', ['create']));
    $method->setDocBlock($docBlock);
    $method->setParameter(['name' => lcfirst($structure['class']), 'type' => $structure['class'] . 'Interface']);
    $method->setReturnType('void');
    $body = <<< EOT
\$this->mapper->create(\$$__class);
EOT;
    $method->setBody($body);
    $class->addMethodFromGenerator($method);

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setName(implode('', ['update']));
    $method->setDocBlock($docBlock);
    $method->setParameter(['name' => lcfirst($structure['class']), 'type' => $structure['class'] . 'Interface']);
    $method->setReturnType('void');
    $body = <<< EOT
\$this->mapper->update(\$$__class);
EOT;
    $method->setBody($body);
    $class->addMethodFromGenerator($method);

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setName(implode('', ['delete']));
    $method->setDocBlock($docBlock);
    $method->setParameter(['name' => lcfirst($structure['class']), 'type' => $structure['class'] . 'Interface']);
    $method->setReturnType('void');
    $body = <<< EOT
\$this->mapper->delete(\$$__class);
EOT;
    $method->setBody($body);
    $class->addMethodFromGenerator($method);

    if ($structure['fields']['id']['type'] == 'uuid') {
        $method = new \Zend\Code\Generator\MethodGenerator();
        $method->setName(implode('', ['nextIdentifier']));
        $method->setDocBlock($docBlock);
        $method->setReturnType('IdentifierInterface');
        $body = <<< EOT
return Uuid4Identifier::fromString(Uuid::uuid4()->toString());
EOT;
        $method->setBody($body);
        $class->addMethodFromGenerator($method);
    }

    $file = new \Zend\Code\Generator\FileGenerator();
    $file->setBody('declare(strict_types = 1);' . PHP_EOL . PHP_EOL . $class->generate());

    $path = implode(DIRECTORY_SEPARATOR, [DIR_SRC, 'Infrastructure', 'Persistence', 'Repository']);

    @mkdir($path, 0777, true);

    file_put_contents($path . DIRECTORY_SEPARATOR . $class->getName() . PHP, $file->generate());
}