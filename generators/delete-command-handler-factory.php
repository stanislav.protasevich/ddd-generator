<?php

foreach ($structures as $structure)
{
    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setShortDescription(implode(' ', ['Class', 'Delete' . $structure['class'] . 'CommandHandlerFactory']));
    $docBlock->setTag(['name' => 'package', 'description' => implode('\\', [BASE_NAMESPACE, 'Container', 'Domain', 'Model', $structure['class'], 'Command', 'Handler'])]);

    $class = new \Zend\Code\Generator\ClassGenerator();
    $class->setNamespaceName(implode('\\', [BASE_NAMESPACE, 'Container', 'Domain', 'Model', $structure['class'], 'Command', 'Handler']));
    $class->setDocBlock($docBlock);
    $class->setFinal(true);
    $class->addUse(implode('\\', [BASE_NAMESPACE, 'Domain', 'Model', $structure['class'], 'Command', 'Handler', 'Delete' . $structure['class'] . 'CommandHandler']));
    $class->addUse(implode('\\', [BASE_NAMESPACE, 'Domain', 'Model', $structure['class'], $structure['class'] . 'RepositoryInterface']));
    $class->addUse('Psr\Container\ContainerInterface');
    $class->setName('Delete' . $structure['class'] . 'CommandHandlerFactory');

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ParamTag('container', ['ContainerInterface']));
    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ReturnTag('Delete' . $structure['class'] . 'CommandHandler'));

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setName('__invoke');
    $method->setDocBlock($docBlock);
    $method->setParameter(['name' => 'container', 'type' => 'ContainerInterface']);
    $_class = 'Delete' . $structure['class'] . 'CommandHandler';
    $__class = $structure['class'] . 'RepositoryInterface';
    $body = <<< EOT
return new $_class(
    \$container->get($__class::class)
);
EOT;
    $method->setBody($body);
    $class->addMethodFromGenerator($method);

    $file = new \Zend\Code\Generator\FileGenerator();
    $file->setBody('declare(strict_types = 1);' . PHP_EOL . PHP_EOL . $class->generate());

    $path = implode(DIRECTORY_SEPARATOR, [DIR_SRC, 'Container', 'Domain', 'Model', $structure['class'], 'Command', 'Handler']);

    @mkdir($path, 0777, true);

    file_put_contents($path . DIRECTORY_SEPARATOR . $class->getName() . PHP, $file->generate());
}