<?php

foreach ($structures as $structure)
{
    global $mapping;

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setShortDescription(implode(' ', ['Class', 'Create' . $structure['class'] . 'CommandHandlerFactory']));
    $docBlock->setTag(['name' => 'package', 'description' => implode('\\', [BASE_NAMESPACE, 'Container', 'Domain', 'Model', $structure['class'], 'Command', 'Handler'])]);

    $class = new \Zend\Code\Generator\ClassGenerator();
    $class->setNamespaceName(implode('\\', [BASE_NAMESPACE, 'Container', 'Domain', 'Model', $structure['class'], 'Command', 'Handler']));
    $class->setDocBlock($docBlock);
    $class->setFinal(true);
    $class->addUse(implode('\\', [BASE_NAMESPACE, 'Domain', 'Model', $structure['class'], 'Command', 'Handler', 'Create' . $structure['class'] . 'CommandHandler']));
    $class->addUse(implode('\\', [BASE_NAMESPACE, 'Domain', 'Model', $structure['class'], $structure['class'] . 'RepositoryInterface']));
    $class->addUse('Psr\Container\ContainerInterface');
    $class->setName('Create' . $structure['class'] . 'CommandHandlerFactory');

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ParamTag('container', ['ContainerInterface']));
    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ReturnTag('Create' . $structure['class'] . 'CommandHandler'));

    $parts = [];
    $parts[] = '$container->get(' . $structure['class'] . 'RepositoryInterface::class)';

    foreach ($structure['fields'] as $parameter => $field) {
        if (isset($field['reference']) && $field['reference']['table'] !== null) {
            $name = $mapping[$field['reference']['table']];
            $type = ucfirst($name) . 'Interface';

            $class->addUse(implode('\\', [BASE_NAMESPACE, 'Domain', 'Model', ucfirst($name), ucfirst($name) . 'RepositoryInterface']));

            $name = $mapping[$field['reference']['table']];
            $type = ucfirst($name) . 'Interface';

            $parts[] = '    $container->get(' . ucfirst($name) . 'RepositoryInterface::class)';
        } else {
            continue;
        }
    }

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setName('__invoke');
    $method->setDocBlock($docBlock);
    $method->setParameter(['name' => 'container', 'type' => 'ContainerInterface']);
    $_class = 'Create' . $structure['class'] . 'CommandHandler';
    $__class = $structure['class'] . 'Repository';

    $variables = implode(',' . PHP_EOL, $parts);

    $body = <<< EOT
return new $_class(
    {$variables}
);
EOT;
    $method->setBody($body);
    $class->addMethodFromGenerator($method);

    $file = new \Zend\Code\Generator\FileGenerator();
    $file->setBody('declare(strict_types = 1);' . PHP_EOL . PHP_EOL . $class->generate());

    $path = implode(DIRECTORY_SEPARATOR, [DIR_SRC, 'Container', 'Domain', 'Model', $structure['class'], 'Command', 'Handler']);

    @mkdir($path, 0777, true);

    file_put_contents($path . DIRECTORY_SEPARATOR . $class->getName() . PHP, $file->generate());
}