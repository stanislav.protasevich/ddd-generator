<?php

foreach ($structures as $structure)
{
    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setShortDescription(implode(' ', ['Class', 'Doctrine'. $structure['class'] . 'MapperFactory']));
    $docBlock->setTag(['name' => 'package', 'description' => implode('\\', [BASE_NAMESPACE, 'Infrastructure', 'Persistence', 'Doctrine', 'Mapper'])]);

    $class = new \Zend\Code\Generator\ClassGenerator();
    $class->setNamespaceName(implode('\\', [BASE_NAMESPACE, 'Container', 'Infrastructure', 'Persistence', 'Doctrine', 'Mapper']));
    $class->setDocBlock($docBlock);
    $class->setFinal(true);
    $class->addUse(implode('\\', [BASE_NAMESPACE, 'Infrastructure', 'Persistence', 'Doctrine', 'Mapper', 'Doctrine' . $structure['class'] . 'Mapper']));
    $class->addUse('Doctrine\ORM\EntityManagerInterface');
    $class->addUse('Psr\Container\ContainerInterface');
    $class->setName('Doctrine'. $structure['class'] . 'MapperFactory');

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ParamTag('container', ['ContainerInterface']));
    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ReturnTag('Doctrine' . $structure['class'] . 'Mapper'));

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setName('__invoke');
    $method->setDocBlock($docBlock);
    $method->setParameter(['name' => 'container', 'type' => 'ContainerInterface']);
    $_class = $structure['class'] . 'Mapper';
    $body = <<< EOT
return new Doctrine$_class(
    \$container->get(EntityManagerInterface::class)
);
EOT;
    $method->setBody($body);
    $class->addMethodFromGenerator($method);

    $file = new \Zend\Code\Generator\FileGenerator();
    $file->setBody('declare(strict_types = 1);' . PHP_EOL . PHP_EOL . $class->generate());

    $path = implode(DIRECTORY_SEPARATOR, [DIR_SRC, 'Container', 'Infrastructure', 'Persistence', 'Doctrine', 'Mapper']);

    @mkdir($path, 0777, true);

    file_put_contents($path . DIRECTORY_SEPARATOR . $class->getName() . PHP, $file->generate());
}