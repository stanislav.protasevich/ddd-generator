<?php

foreach ($structures as $structure)
{
    global $mapping;

    // Model Interface
    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setShortDescription(implode(' ', ['Interface', $structure['class'] . 'Repository' . ucfirst(INTERFACE_NAME_KEY)]));
    $docBlock->setTag(['name' => 'package', 'description' => implode('\\', [$structure[NAMESPACE_NAME_KEY], $structure['class']])]);

    $interface = new \Zend\Code\Generator\InterfaceGenerator();
    $interface->setNamespaceName(implode('\\', [$structure[NAMESPACE_NAME_KEY], $structure['class']]));
    $interface->setDocBlock($docBlock);
    $interface->addUse(implode('\\', [$structure[NAMESPACE_NAME_KEY], $structure['class']]) . 'Interface');
    $interface->addUse('ProDevZone\Common\Identifier\IdentifierInterface');
    $interface->addUse('InvalidArgumentException');
    $interface->setName($structure['class'] . 'Repository' . ucfirst(INTERFACE_NAME_KEY));

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ParamTag('conditions', ['array']));
    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ReturnTag('array'));

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setDocBlock($docBlock);
    $method->setName(implode('', ['findAllBy']));
    $method->setParameter(['name' => 'conditions', 'type' => 'array', 'defaultValue' => []]);
    $method->setReturnType('array');
    $interface->addMethodFromGenerator($method);

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ParamTag('conditions', ['array']));
    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ReturnTag([$structure['class'] . 'Interface', 'array']));

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setDocBlock($docBlock);
    $method->setName(implode('', ['findOneBy']));
    $method->setParameter(['name' => 'conditions', 'type' => 'array', 'defaultValue' => []]);
    $method->setReturnType('?'. $structure['class'] . 'Interface');
    $interface->addMethodFromGenerator($method);

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ParamTag('conditions', ['array']));
    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ThrowsTag('InvalidArgumentException'));
    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ReturnTag($structure['class'] . 'Interface'));

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setName(implode('', ['findOneOrFail']));
    $method->setDocBlock($docBlock);
    $method->setParameter(['name' => 'conditions', 'type' => 'array', 'defaultValue' => []]);
    $method->setReturnType($structure['class'] . 'Interface');
    $interface->addMethodFromGenerator($method);

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ParamTag(lcfirst($structure['class']), [$structure['class'] . 'Interface']));

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setName(implode('', ['create']));
    $method->setDocBlock($docBlock);
    $method->setParameter(['name' => lcfirst($structure['class']), 'type' => $structure['class'] . 'Interface']);
    $method->setReturnType('void');
    $interface->addMethodFromGenerator($method);

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setName(implode('', ['update']));
    $method->setDocBlock($docBlock);
    $method->setParameter(['name' => lcfirst($structure['class']), 'type' => $structure['class'] . 'Interface']);
    $method->setReturnType('void');
    $interface->addMethodFromGenerator($method);

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setName(implode('', ['delete']));
    $method->setDocBlock($docBlock);
    $method->setParameter(['name' => lcfirst($structure['class']), 'type' => $structure['class'] . 'Interface']);
    $method->setReturnType('void');
    $interface->addMethodFromGenerator($method);

    if ($structure['fields']['id']['type'] == 'uuid') {
        $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
        $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ReturnTag('IdentifierInterface'));

        $method = new \Zend\Code\Generator\MethodGenerator();
        $method->setName(implode('', ['nextIdentifier']));
        $method->setDocBlock($docBlock);
        $method->setReturnType('IdentifierInterface');
        $interface->addMethodFromGenerator($method);
    }

    $file = new \Zend\Code\Generator\FileGenerator();
    $file->setBody('declare(strict_types = 1);' . PHP_EOL . PHP_EOL . $interface->generate());

    $path = implode(DIRECTORY_SEPARATOR, [DIR_SRC, $structure[CONTEXT_NAME_KEY], DOMAIN_NAME_KEY, MODEL_NAME_KEY, $structure['class'], $interface->getName()]);
    file_put_contents($path . PHP, $file->generate());
}