<?php

foreach ($structures as $structure)
{
    global $mapping;
    global $constructParams;

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setShortDescription(implode(' ', ['Class', 'Delete' . $structure['class'] . 'CommandHandler']));
    $docBlock->setTag(['name' => 'package', 'description' => implode('\\', [BASE_NAMESPACE, 'Domain', 'Model', $structure['class'], 'Command', 'Handler'])]);

    $class = new \Zend\Code\Generator\ClassGenerator();
    $class->setFinal(true);
    $class->setNamespaceName(implode('\\', [BASE_NAMESPACE, 'Domain', 'Model', $structure['class'], 'Command', 'Handler']));
    $class->setDocBlock($docBlock);
    $class->setName('Delete' . $structure['class'] . 'CommandHandler');
    $class->addUse(implode('\\', [BASE_NAMESPACE, 'Domain', 'Model', $structure['class'], 'Command', 'Delete' . $structure['class'] . 'CommandInterface']));

    $parts = [];
    foreach ($structure['fields'] as $parameter => $field) {
        if ($parameter != 'id') {
            continue;
        }

        $name = lcfirst($structure['class']) . 'Repository';
        $type = ucfirst($name) . 'Interface';

        $class->addUse(implode('\\', [BASE_NAMESPACE, 'Domain', 'Model', $structure['class'], $structure['class'] . 'RepositoryInterface']));

        $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
        $docBlock->setTag(
            (new \Zend\Code\Generator\DocBlock\Tag\VarTag(
                $name,
                [$type]
            ))
        );

        $property = new \Zend\Code\Generator\PropertyGenerator();
        $property->setDocBlock($docBlock);
        $property->setName($name);
        $property->setVisibility(\Zend\Code\Generator\PropertyGenerator::VISIBILITY_PRIVATE);
        $class->addPropertyFromGenerator($property);
    }

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setShortDescription(implode(' ', ['Delete' . $structure['class'] . 'CommandHandler', 'constructor.']));

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setName(implode('', ['__construct']));
    $parts = [];
    foreach ($structure['fields'] as $parameter => $field) {
        if ($parameter != 'id') {
            continue;
        }

        $name = lcfirst($structure['class']) . 'Repository';
        $type = ucfirst($name) . 'Interface';

        if (isset($field['reference']) && $field['reference']['table'] !== null) {
            //$interface->addUse($field['namespace'] . INTERFACE_NAME_KEY);

            $name = $mapping[$field['reference']['table']] . 'Identifier';
            $type = 'string';
        }

        $parameter = new \Zend\Code\Generator\ParameterGenerator();
        $parameter->setName($name);
        $parameter->setType($type);

        $method->setParameter($parameter);

        $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ParamTag($name, [$type]));

        $parts[] = '$this->' . $name . ' = $' . $name. ';';
    }
    $method->setDocBlock($docBlock);
    $method->setBody(implode(PHP_EOL, $parts));
    $class->addMethodFromGenerator($method);

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ParamTag('command', [implode(' ', ['Delete' . $structure['class'] . 'CommandInterface'])]));

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setName('__invoke');
    $method->setDocBlock($docBlock);

    foreach ($structure['fields'] as $parameter => $field) {
        if ($parameter != 'id') {
            continue;
        }

        $parameter = 'Identifier';

        $name = 'command';
        $type = 'Delete' . $structure['class'] . 'CommandInterface';

        $parameter = new \Zend\Code\Generator\ParameterGenerator();
        $parameter->setName($name);
        $parameter->setType($type);

        $method->setParameter($parameter);
    }

    $_repository = lcfirst($structure['class']);
    $body = <<< EOT
\${$_repository} = \$this->{$_repository}Repository->findOneOrFail(
    [
        'id' => \$command->{$_repository}Identifier()
    ]
);

\$this->{$_repository}Repository->delete(\${$_repository});
EOT;
    $method->setBody($body);
    $class->addMethodFromGenerator($method);

    $file = new \Zend\Code\Generator\FileGenerator();
    $file->setBody('declare(strict_types = 1);' . PHP_EOL . PHP_EOL . $class->generate());

    $path = implode(DIRECTORY_SEPARATOR, [DIR_SRC, 'Domain', 'Model', $structure['class'], 'Command', 'Handler']);

    @mkdir($path, 0777, true);

    file_put_contents($path . DIRECTORY_SEPARATOR . $class->getName() . PHP, $file->generate());
}