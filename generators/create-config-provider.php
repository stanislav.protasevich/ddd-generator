<?php

$docBlock = new \Zend\Code\Generator\DocBlockGenerator();
$docBlock->setShortDescription(implode(' ', ['The configuration provider']));
$docBlock->setTag(['name' => 'package', 'description' => implode('\\', [BASE_NAMESPACE, 'Api'])]);

$class = new \Zend\Code\Generator\ClassGenerator();
$class->setFinal(true);
$class->setNamespaceName(implode('\\', [BASE_NAMESPACE, 'Api']));
$class->setDocBlock($docBlock);
$class->setName('ConfigProvider');

/* __invoke method */

$docBlock = new \Zend\Code\Generator\DocBlockGenerator();
$docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ReturnTag('array'));

$method = new \Zend\Code\Generator\MethodGenerator();
$method->setDocBlock($docBlock);
$method->setName('__invoke');

$body = <<< EOT
return [
    'dependencies' => \$this->getDependencies(),
];
EOT;
$method->setBody($body);
$class->addMethodFromGenerator($method);

$factories = [];

$factories[] = '/* Controller actions */';
foreach ($structures as $structure)
{
    $factories[] = '        Create' . $structure['class'] . 'Action::class => Create' . $structure['class'] . 'ActionFactory::class,';
    $factories[] = '        Update' . $structure['class'] . 'Action::class => Update' . $structure['class'] . 'ActionFactory::class,';
    $factories[] = '        Delete' . $structure['class'] . 'Action::class => Delete' . $structure['class'] . 'ActionFactory::class,';

    $class->addUse(implode('\\', [BASE_NAMESPACE, 'Api', 'Action', $structure['class'], 'Create' . $structure['class'] . 'Action']));
    $class->addUse(implode('\\', [BASE_NAMESPACE, 'Api', 'Action', $structure['class'], 'Update' . $structure['class'] . 'Action']));
    $class->addUse(implode('\\', [BASE_NAMESPACE, 'Api', 'Action', $structure['class'], 'Delete' . $structure['class'] . 'Action']));

    $class->addUse(implode('\\', [BASE_NAMESPACE, 'Container', 'Api', 'Action', $structure['class'], 'Create' . $structure['class'] . 'ActionFactory']));
    $class->addUse(implode('\\', [BASE_NAMESPACE, 'Container', 'Api', 'Action', $structure['class'], 'Update' . $structure['class'] . 'ActionFactory']));
    $class->addUse(implode('\\', [BASE_NAMESPACE, 'Container', 'Api', 'Action', $structure['class'], 'Delete' . $structure['class'] . 'ActionFactory']));
}

$factories[] = '';

$factories[] = '        /* Command handlers */';
foreach ($structures as $structure)
{
    $factories[] = '        Create' . $structure['class'] . 'CommandHandler::class => Create' . $structure['class'] . 'CommandHandlerFactory::class,';
    $factories[] = '        Update' . $structure['class'] . 'CommandHandler::class => Update' . $structure['class'] . 'CommandHandlerFactory::class,';
    $factories[] = '        Delete' . $structure['class'] . 'CommandHandler::class => Delete' . $structure['class'] . 'CommandHandlerFactory::class,';

    $class->addUse(implode('\\', [BASE_NAMESPACE, 'Domain', 'Model', $structure['class'], 'Command', 'Handler', 'Create' . $structure['class'] . 'CommandHandler']));
    $class->addUse(implode('\\', [BASE_NAMESPACE, 'Domain', 'Model', $structure['class'], 'Command', 'Handler', 'Update' . $structure['class'] . 'CommandHandler']));
    $class->addUse(implode('\\', [BASE_NAMESPACE, 'Domain', 'Model', $structure['class'], 'Command', 'Handler', 'Delete' . $structure['class'] . 'CommandHandler']));

    $class->addUse(implode('\\', [BASE_NAMESPACE, 'Container', 'Domain', 'Model', $structure['class'], 'Command', 'Handler', 'Create' . $structure['class'] . 'CommandHandlerFactory']));
    $class->addUse(implode('\\', [BASE_NAMESPACE, 'Container', 'Domain', 'Model', $structure['class'], 'Command', 'Handler', 'Update' . $structure['class'] . 'CommandHandlerFactory']));
    $class->addUse(implode('\\', [BASE_NAMESPACE, 'Container', 'Domain', 'Model', $structure['class'], 'Command', 'Handler', 'Delete' . $structure['class'] . 'CommandHandlerFactory']));
}

$factories[] = '';

$factories[] = '        /* Command handlers */';
foreach ($structures as $structure)
{
    $factories[] = '        ' . $structure['class'] . 'MapperInterface::class => Doctrine' . $structure['class'] . 'MapperFactory::class,';

    $class->addUse(implode('\\', [BASE_NAMESPACE, 'Domain', 'Model', $structure['class'], $structure['class'] . 'MapperInterface']));

    $class->addUse(implode('\\', [BASE_NAMESPACE, 'Container', 'Infrastructure', 'Persistence', 'Doctrine', 'Mapper', 'Doctrine' . $structure['class'] . 'MapperFactory']));
}

$factories[] = '';

$factories[] = '        /* Repositories */';
foreach ($structures as $structure)
{
    $factories[] = '        ' . $structure['class'] . 'RepositoryInterface::class => ' . $structure['class'] . 'RepositoryFactory::class,';

    $class->addUse(implode('\\', [BASE_NAMESPACE, 'Domain', 'Model', $structure['class'], $structure['class'] . 'RepositoryInterface']));

    $class->addUse(implode('\\', [BASE_NAMESPACE, 'Container', 'Infrastructure', 'Persistence', 'Repository', $structure['class'] . 'RepositoryFactory']));
}

/* getDependencies method */

$docBlock = new \Zend\Code\Generator\DocBlockGenerator();
$docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ReturnTag('array'));

$method = new \Zend\Code\Generator\MethodGenerator();
$method->setDocBlock($docBlock);
$method->setName('getDependencies');

$_factories = implode(PHP_EOL, $factories);

$body = <<< EOT
return [
    'factories' => [
        {$_factories}
    ]
];
EOT;
$method->setBody($body);
$class->addMethodFromGenerator($method);

$file = new \Zend\Code\Generator\FileGenerator();
$file->setBody('declare(strict_types = 1);' . PHP_EOL . PHP_EOL . $class->generate());

$path = implode(DIRECTORY_SEPARATOR, [DIR_SRC, 'Api']);

@mkdir($path, 0777, true);

file_put_contents($path . DIRECTORY_SEPARATOR . $class->getName() . PHP, $file->generate());