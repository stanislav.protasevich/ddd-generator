<?php

class Folder
{
    private $package = null;

    public function __construct($package)
    {
        if (is_null($package)) {
            throw new \Exception('Invalid Package"' . $package .'"');
        }

        $this->setPackage($package);
    }

    public function setPackage($package)
    {
        $this->package = $package;
    }

    public function generateFolderStructure()
    {
        
    }
}