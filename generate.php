<?php

require_once 'vendor/autoload.php';

define('DIR_SRC', 'src_xmpl');
define('DIR_CORE', 'Core');
define('DIR_DOMAIN', 'Domain');
define('DIR_ENTITY', 'Entity');
define('DIR_FIELD', 'Field');
define('DIR_EXCEPTION', 'Exception');

define('TYPE_CLASS', 'class');
define('TYPE_INTERFACE', 'interface');

define('DOC_TYPE_CLASS', 'Class');
define('DOC_TYPE_INTERFACE', 'Interface');

define('PHP', '.php');

define('PACKAGE', 'Templater');
define('INVALID', 'Invalid');

$int = [
    'tinyint' => [
        0 => ['min' => -128, 'max' => 127],
        1 => ['min' => 0, 'max' => 255],
    ],
    'smallint' => [
        0 => ['min' => -32768, 'max' => 32767],
        1 => ['min' => 0, 'max' => 65535],
    ],
    'mediumint' => [
        0 => ['min' => -8388608, 'max' => 8388607],
        1 => ['min' => 0, 'max' => 16777215],
    ],
    'int' => [
        0 => ['min' => -2147483648, 'max' => 2147483647],
        1 => ['min' => 0, 'max' => 4294967295],
    ],
    'bigint' => [
        0 => ['min' => -9223372036854775808, 'max' => 9223372036854775807],
        1 => ['min' => 0, 'max' => 18446744073709551615],
    ],
];

//function dashesToCamelCase($string, $capitalizeFirstCharacter = false)
//{
//    $str = str_replace('-', '', ucwords($string, '-'));
//
//    if (!$capitalizeFirstCharacter) {
//        $str = lcfirst($str);
//    }
//
//    return $str;
//}

$package = 'Templater';
$table = 'user';

$db = new \PDO('mysql:host=localhost;dbname=generator', 'root', 'root');

$blueprint = ['package' => $package, 'table' => $table];
$result = $db->query("DESCRIBE " . $table)->fetchAll();

foreach ($result as $column) {
    $field = [];

    $field['field'] = $column['Field'];

    // Get type
    $type = null;
    if (strpos($column['Type'], 'tinyint') !== false) {
        $type = 'tinyint';
    } else if (strpos($column['Type'], 'smallint') !== false) {
        $type = 'smallint';
    } else if (strpos($column['Type'], 'mediumint') !== false) {
        $type = 'mediumint';
    } else if (strpos($column['Type'], 'bigint') !== false) {
        $type = 'bigint';
    } else if (strpos($column['Type'], 'int') !== false) {
        $type = 'int';
    } else if (strpos($column['Type'], 'varchar') !== false) {
        $type = 'varchar';
    } else if (strpos($column['Type'], 'char') !== false) {
        $type = 'char';
    } else if (strpos($column['Type'], 'text') !== false) {
        $type = 'text';
    }

    if (is_null($type)) {
        //throw new \Exception('Unknown column type');
    }

    $unsigned = false;
    if (strpos($column['Type'], 'unsigned') !== false) {
        $unsigned = true;
    }

//    $extra = null;
//    if (!empty($column['Extra'])) {
//        $extra = $column['Extra'];
//    }

    $field['type'] = $type;

    // Get null
    $null = false;
    if ($column['Null'] == 'YES') {
        $null = true;
    }

    $field['nullable'] = $null;

    // Set validators

    if (!$null) {
        $field['validators'][] = ['validator' => '\Zend\Validator\NotEmpty', 'options' => []];
    }

    if (in_array($type, ['tinyint', 'smallint', 'mediumint', 'int', 'bigint'])) {
        $field['php_type'] = 'int';

        $field['validators'][] = ['validator' => '\Zend\I18n\Validator\IsInt', 'options' => []];

        $field['validators'][] = [
            'validator' => '\Zend\Validator\Between',
            'options' => ['min' => $int[$type][$unsigned]['min'], 'max' => $int[$type][$unsigned]['max']],
        ];
    } else if (in_array($type, ['char', 'varchar', 'text'])) {
        $field['php_type'] = 'string';

        $field['validators'] = [];
    }

    $blueprint['fields'][] = $field;

//    break;
}

//$blueprint['table'] = dashesToCamelCase($blueprint['table'], true);
//foreach ($blueprint['fields'] as $key => $field) {
//    $blueprint['fields'][$key]['u_field'] = dashesToCamelCase($field['field'], true);
//    $blueprint['fields'][$key]['l_field'] = dashesToCamelCase($field['field']);
//    $blueprint['fields'][$key]['field'] = str_replace('_', ' ', $field['field']);
//}

//print_r($blueprint);

function generateCoreObjects(&$blueprint)
{
    /* ENTITY FIELD CORE INTERFACE */

    $namespace = implode('\\', [PACKAGE, DIR_CORE, DIR_ENTITY, DIR_FIELD]);

    $entityFieldInterfacePath = implode(DIRECTORY_SEPARATOR, [DIR_SRC, DIR_CORE, DIR_ENTITY, DIR_FIELD]);
    $entityFieldInterfaceFile = DIR_ENTITY . DIR_FIELD . DOC_TYPE_INTERFACE;

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setShortDescription(implode(' ', [DOC_TYPE_INTERFACE, $entityFieldInterfaceFile]));
    $docBlock->setTag(['name' => 'package', 'description' => $namespace]);

    $interface = new \Zend\Code\Generator\InterfaceGenerator();
    $interface->setNamespaceName($namespace);
    $interface->setDocBlock($docBlock);
    $interface->setName($entityFieldInterfaceFile);

    // setFilterChain
    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setName('setFilterChain');
    $interface->addMethodFromGenerator($method);

    // getFilterChain
    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setName('getFilterChain');
    $interface->addMethodFromGenerator($method);

    // setValidatorChain
    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setName('setValidatorChain');
    $interface->addMethodFromGenerator($method);

    // getValidatorChain
    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setName('getValidatorChain');
    $interface->addMethodFromGenerator($method);

    $file = new \Zend\Code\Generator\FileGenerator();
    $file->setClass($interface);

    @mkdir($entityFieldInterfacePath, 0777, true);
    file_put_contents(implode(DIRECTORY_SEPARATOR, [$entityFieldInterfacePath, $entityFieldInterfaceFile . PHP]), $file->generate());

    $blueprint['core']['entity']['interface']['namespace'] = $namespace;
    $blueprint['core']['entity']['interface']['path'] = $entityFieldInterfacePath;
    $blueprint['core']['entity']['interface']['file'] = $entityFieldInterfaceFile;

    /* ENTITY FIELD CORE ABSTRACT CLASS */

    $namespace = implode('\\', [PACKAGE, DIR_CORE, DIR_ENTITY, DIR_FIELD]);

    $entityFieldClassPath = implode(DIRECTORY_SEPARATOR, [DIR_SRC, DIR_CORE, DIR_ENTITY, DIR_FIELD]);
    $entityFieldClassFile = DIR_ENTITY . DIR_FIELD;

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setShortDescription(implode(' ', [DOC_TYPE_CLASS, $entityFieldClassFile]));
    $docBlock->setTag(['name' => 'package', 'description' => $namespace]);

    $class = new \Zend\Code\Generator\ClassGenerator();
    $class->setAbstract(true);
    $class->setNamespaceName($namespace);
    $class->setDocBlock($docBlock);
    $class->setName($entityFieldClassFile);

    $class->setImplementedInterfaces([implode('\\', [$blueprint['core']['entity']['interface']['namespace'], $blueprint['core']['entity']['interface']['file']])]);

    // Uses
    $class->addUse('Zend\Filter\FilterChain');
    $class->addUse('Zend\Validator\ValidatorChain');

    // Properties
    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setShortDescription('Entity Filter Chain');
    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\GenericTag('var', implode('|', ['null', 'FilterChain'])));

    $property = new \Zend\Code\Generator\PropertyGenerator('filterChain', null, \Zend\Code\Generator\PropertyGenerator::FLAG_PROTECTED);
    $property->setDocBlock($docBlock);

    $class->addPropertyFromGenerator($property);

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setShortDescription('Entity Validator Chain');
    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\GenericTag('var', implode('|', ['null', 'ValidatorChain'])));

    $property = new \Zend\Code\Generator\PropertyGenerator('validatorChain', null, \Zend\Code\Generator\PropertyGenerator::FLAG_PROTECTED);
    $property->setDocBlock($docBlock);

    $class->addPropertyFromGenerator($property);

    // Construct
    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setShortDescription('EntityField constructor');

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setDocBlock($docBlock);
    $method->setName('__construct');
    $method->setBody(
        '$this->filterChain = new FilterChain();' . PHP_EOL .
        '$this->validatorChain = new ValidatorChain();' . PHP_EOL .
        PHP_EOL .
        '$this->setFilterChain();' . PHP_EOL .
        '$this->setValidatorChain();' . PHP_EOL
    );
    $class->addMethodFromGenerator($method);

    // Methods
//    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
//    $docBlock->setShortDescription('Set entity filter chain');
//    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ThrowsTag('\Exception'));
//
//    $method = new \Zend\Code\Generator\MethodGenerator();
//    $method->setDocBlock($docBlock);
//    $method->setName('setFilterChain');
//    $method->setBody(
//        'throw new \Exception(\'Empty filter chain\');' . PHP_EOL
//    );
//    $class->addMethodFromGenerator($method);

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setShortDescription('Get entity filter chain');
    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ReturnTag(implode('|', ['null', 'FilterChain'])));

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setDocBlock($docBlock);
    $method->setName('getFilterChain');
    $method->setBody(
        'return $this->filterChain;' . PHP_EOL
    );
    $class->addMethodFromGenerator($method);

//    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
//    $docBlock->setShortDescription('Set entity validator chain');
//    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ThrowsTag('\Exception'));
//
//    $method = new \Zend\Code\Generator\MethodGenerator();
//    $method->setDocBlock($docBlock);
//    $method->setName('setValidatorChain');
//    $method->setBody(
//        'throw new \Exception(\'Empty validator chain\');' . PHP_EOL
//    );
//    $class->addMethodFromGenerator($method);

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setShortDescription('Get entity validator chain');
    $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ReturnTag(implode('|', ['null', 'ValidatorChain'])));

    $method = new \Zend\Code\Generator\MethodGenerator();
    $method->setDocBlock($docBlock);
    $method->setName('getValidatorChain');
    $method->setBody(
        'return $this->validatorChain;' . PHP_EOL
    );
    $class->addMethodFromGenerator($method);

    $file = new \Zend\Code\Generator\FileGenerator();
    $file->setClass($class);

    @mkdir($entityFieldInterfacePath, 0777, true);
    file_put_contents(implode(DIRECTORY_SEPARATOR, [$entityFieldClassPath, $entityFieldClassFile . PHP]), $file->generate());

    $blueprint['core']['entity']['class']['namespace'] = $namespace;
    $blueprint['core']['entity']['class']['path'] = $entityFieldClassPath;
    $blueprint['core']['entity']['class']['file'] = $entityFieldClassFile;
}

function generateEntityFieldExceptions(&$blueprint)
{
    $entity = \Zend\Filter\StaticFilter::execute(ucfirst($blueprint['table']), \Zend\Filter\Word\UnderscoreToCamelCase::class);

    $namespace = implode('\\', [PACKAGE, DIR_DOMAIN, DIR_ENTITY, $entity, DIR_FIELD, DIR_EXCEPTION]);

    $exceptionClassPath = implode(DIRECTORY_SEPARATOR, [DIR_SRC, DIR_DOMAIN, DIR_ENTITY, $entity, DIR_FIELD, DIR_EXCEPTION]);

    foreach($blueprint['fields'] as &$field) {
        $uFieldName = \Zend\Filter\StaticFilter::execute($field['field'], \Zend\Filter\Word\UnderscoreToCamelCase::class);

        $exceptionClassFile = implode('', [INVALID, $entity, $uFieldName, DIR_FIELD, DIR_EXCEPTION]);

        $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
        $docBlock->setShortDescription(implode(' ', [DOC_TYPE_CLASS, $exceptionClassFile]));
        $docBlock->setTag(['name' => 'package', 'description' => $namespace]);

        $class = new \Zend\Code\Generator\ClassGenerator();
        $class->setNamespaceName($namespace);
        $class->setDocBlock($docBlock);
        $class->setName($exceptionClassFile);
        $class->setExtendedClass('InvalidArgumentException');

        $file = new \Zend\Code\Generator\FileGenerator();
        $file->setClass($class);

        @mkdir($exceptionClassPath, 0777, true);
        file_put_contents(implode(DIRECTORY_SEPARATOR, [$exceptionClassPath, $exceptionClassFile . PHP]), $file->generate());

        $blueprint['domain']['entity'][$blueprint['table']][$field['field']]['exception']['namespace'] = $namespace;
        $blueprint['domain']['entity'][$blueprint['table']][$field['field']]['exception']['path'] = $exceptionClassPath;
        $blueprint['domain']['entity'][$blueprint['table']][$field['field']]['exception']['file'] = $exceptionClassFile;
    }
}

function generateEntityFieldInterfaces(&$blueprint)
{
    $entity = \Zend\Filter\StaticFilter::execute(ucfirst($blueprint['table']), \Zend\Filter\Word\UnderscoreToCamelCase::class);
    $studlyEntity = \Zend\Filter\StaticFilter::execute(ucfirst($blueprint['table']), \Zend\Filter\Word\UnderscoreToStudlyCase::class);

    $namespace = implode('\\', [PACKAGE, DIR_DOMAIN, DIR_ENTITY, $entity, DIR_FIELD]);

    $fieldInterfacePath = implode(DIRECTORY_SEPARATOR, [DIR_SRC, DIR_DOMAIN, DIR_ENTITY, $entity, DIR_FIELD]);

    foreach($blueprint['fields'] as &$field) {
        $uFieldName = \Zend\Filter\StaticFilter::execute($field['field'], \Zend\Filter\Word\UnderscoreToCamelCase::class);
        $lFieldName = lcfirst(\Zend\Filter\StaticFilter::execute($field['field'], \Zend\Filter\Word\UnderscoreToCamelCase::class));
        $studlyFieldName = \Zend\Filter\StaticFilter::execute($field['field'], \Zend\Filter\Word\UnderscoreToStudlyCase::class);
        $baseInterface = implode('\\', [$blueprint['core']['entity']['interface']['namespace'], $blueprint['core']['entity']['interface']['file']]);

        $fieldInterfaceFile = implode('', [$entity, $uFieldName, DIR_FIELD, DOC_TYPE_INTERFACE]);

        $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
        $docBlock->setShortDescription(implode(' ', [DOC_TYPE_INTERFACE, $fieldInterfaceFile]));
        $docBlock->setTag(['name' => 'package', 'description' => $namespace]);

        $interface = new \Zend\Code\Generator\InterfaceGenerator();
        $interface->setNamespaceName($namespace);
        $interface->addUse($baseInterface);
        $interface->setDocBlock($docBlock);
        $interface->setName($fieldInterfaceFile . '__EXTENDS__');
        $interface->setExtendedClass($baseInterface);

        // Methods
        $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
        $docBlock->setShortDescription(implode(' ', ['Set', $studlyEntity, $studlyFieldName]));
        $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ParamTag($field['field']));
        $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ThrowsTag(implode('\\', [DIR_EXCEPTION, $blueprint['domain']['entity'][$blueprint['table']][$field['field']]['exception']['file']])));

        $method = new \Zend\Code\Generator\MethodGenerator();
        $method->setDocBlock($docBlock);
        $method->setName(implode('', ['set', $uFieldName]));
        $method->setParameter($lFieldName);
        $interface->addMethodFromGenerator($method);

        $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
        $docBlock->setShortDescription(implode(' ', ['Get', $studlyEntity, $studlyFieldName]));
        $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ReturnTag(implode('|', ['null', $field['php_type']])));

        $method = new \Zend\Code\Generator\MethodGenerator();
        $method->setDocBlock($docBlock);
        $method->setName($field['field']);
        $interface->addMethodFromGenerator($method);

        $file = new \Zend\Code\Generator\FileGenerator();
        $file->setClass($interface);

        @mkdir($fieldInterfacePath, 0777, true);
        file_put_contents(implode(DIRECTORY_SEPARATOR, [$fieldInterfacePath, $fieldInterfaceFile . PHP]), str_replace('__EXTENDS__', ' extends ' . $blueprint['core']['entity']['interface']['file'], $file->generate()));

        $blueprint['domain']['entity'][$blueprint['table']][$field['field']]['interface']['namespace'] = $namespace;
        $blueprint['domain']['entity'][$blueprint['table']][$field['field']]['interface']['path'] = $fieldInterfacePath;
        $blueprint['domain']['entity'][$blueprint['table']][$field['field']]['interface']['file'] = $fieldInterfaceFile;
    }
}

function generateEntityFields(&$blueprint) {
    $entity = \Zend\Filter\StaticFilter::execute(ucfirst($blueprint['table']), \Zend\Filter\Word\UnderscoreToCamelCase::class);
    $studlyEntity = \Zend\Filter\StaticFilter::execute(ucfirst($blueprint['table']), \Zend\Filter\Word\UnderscoreToStudlyCase::class);

    $namespace = implode('\\', [PACKAGE, DIR_DOMAIN, DIR_ENTITY, $entity, DIR_FIELD]);

    $fieldClassPath = implode(DIRECTORY_SEPARATOR, [DIR_SRC, DIR_DOMAIN, DIR_ENTITY, $entity, DIR_FIELD]);

    foreach($blueprint['fields'] as &$field) {
        $uFieldName = \Zend\Filter\StaticFilter::execute($field['field'], \Zend\Filter\Word\UnderscoreToCamelCase::class);
        $lFieldName = lcfirst(\Zend\Filter\StaticFilter::execute($field['field'], \Zend\Filter\Word\UnderscoreToCamelCase::class));
        $studlyFieldName = \Zend\Filter\StaticFilter::execute($field['field'], \Zend\Filter\Word\UnderscoreToStudlyCase::class);
        $baseClass = implode('\\', [$blueprint['core']['entity']['class']['namespace'], $blueprint['core']['entity']['class']['file']]);

        $fieldClassFile = implode('', [$entity, $uFieldName, DIR_FIELD]);

        $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
        $docBlock->setShortDescription(implode(' ', [DOC_TYPE_CLASS, $fieldClassFile]));
        $docBlock->setTag(['name' => 'package', 'description' => $namespace]);

        $class = new \Zend\Code\Generator\ClassGenerator();
        $class->setNamespaceName($namespace);
        $class->addUse($baseClass);
        $class->setDocBlock($docBlock);
        $class->setName($fieldClassFile);
        $class->setExtendedClass($baseClass);
        $class->setImplementedInterfaces([implode('\\', [$blueprint['domain']['entity'][$blueprint['table']][$field['field']]['interface']['namespace'], $blueprint['domain']['entity'][$blueprint['table']][$field['field']]['interface']['file']])]);

        // Properties
        $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
        $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\GenericTag('var', implode(' ', [implode('|', ['null', $field['php_type']]), ucfirst($studlyEntity), $studlyFieldName])));

        $property = new \Zend\Code\Generator\PropertyGenerator($lFieldName, null, \Zend\Code\Generator\PropertyGenerator::FLAG_PRIVATE);
        $property->setDocBlock($docBlock);

        $class->addPropertyFromGenerator($property);

        // Methods
        $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
        $docBlock->setShortDescription(implode(' ', [ucfirst($studlyEntity), $studlyFieldName, 'constructor']));
        $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ParamTag($field['field'], ['null', $field['php_type']]));

        $method = new \Zend\Code\Generator\MethodGenerator();
        $method->setDocBlock($docBlock);
        $method->setName('__construct');
        $method->setParameter($lFieldName . ' = null');
        $method->setBody(
            'parent::__construct();' . PHP_EOL .
            PHP_EOL .
            '$this->set' . $uFieldName . '($' . $lFieldName . ');' . PHP_EOL
        );
        $class->addMethodFromGenerator($method);

        $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
        $docBlock->setShortDescription(implode(' ', ['Set', $studlyEntity, $studlyFieldName]));
        $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ParamTag($field['field']));
        $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ThrowsTag(implode('\\', [DIR_EXCEPTION, $blueprint['domain']['entity'][$blueprint['table']][$field['field']]['exception']['file']])));

        $method = new \Zend\Code\Generator\MethodGenerator();
        $method->setDocBlock($docBlock);
        $method->setName(implode('', ['set', $uFieldName]));
        $method->setParameter($lFieldName);
        $method->setBody(
            '$' . $lFieldName . ' = $this->filterChain->filter($' . $lFieldName . ');' . PHP_EOL .
            PHP_EOL .
            'if (!$this->validatorChain->isValid($' . $lFieldName . ')) {' . PHP_EOL .
            '    throw new ' . DIR_EXCEPTION . '\\' . $blueprint['domain']['entity'][$blueprint['table']][$field['field']]['exception']['file'] . '($' . $lFieldName . ');' . PHP_EOL .
            '}' . PHP_EOL .
            PHP_EOL .
            '$this->' . $lFieldName .' = $' . $lFieldName . ';' . PHP_EOL
        );
        $class->addMethodFromGenerator($method);

        $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
        $docBlock->setShortDescription(implode(' ', ['Get', $studlyEntity, $studlyFieldName]));
        $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\ReturnTag(implode('|', ['null', $field['php_type']])));

        $method = new \Zend\Code\Generator\MethodGenerator();
        $method->setDocBlock($docBlock);
        $method->setName($field['field']);
        $method->setBody('return $this->' . $lFieldName . ';');
        $class->addMethodFromGenerator($method);

        $method = new \Zend\Code\Generator\MethodGenerator();
        $method->setName('setFilterChain');
        $body = '' . PHP_EOL;
        $method->setBody($body);
        $class->addMethodFromGenerator($method);

        $method = new \Zend\Code\Generator\MethodGenerator();
        $method->setName('setValidatorChain');
        $body = '' . PHP_EOL;
        $method->setBody($body);
        $class->addMethodFromGenerator($method);

        $file = new \Zend\Code\Generator\FileGenerator();
        $file->setClass($class);

        @mkdir($fieldClassPath, 0777, true);
        file_put_contents(implode(DIRECTORY_SEPARATOR, [$fieldClassPath, $fieldClassFile . PHP]), $file->generate());

        $blueprint['domain']['entity'][$blueprint['table']][$field['field']]['namespace'] = $namespace;
        $blueprint['domain']['entity'][$blueprint['table']][$field['field']]['path'] = $fieldClassPath;
        $blueprint['domain']['entity'][$blueprint['table']][$field['field']]['file'] = $fieldClassFile;
    }
}

function generateEntity($blueprint) {
    $namespace = implode('\\', [$blueprint['package'], 'Domain', 'Entity', $blueprint['table']]);
    $className = $blueprint['table'] . 'Entity';
    $entity = $blueprint['table'];
    $l_entity = strtolower($blueprint['table']);

    $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
    $docBlock->setShortDescription('Class ' . $className);
    $docBlock->setTag(['name' => 'package', 'description' => $namespace]);

    $class = new \Zend\Code\Generator\ClassGenerator();
    $class->setNamespaceName($namespace);
    $class->setDocBlock($docBlock);
    $class->setName($className);

    foreach($blueprint['fields'] as $field) {
        $fieldName = $field['field'];

        $fieldType = $field['type'];
        if (isset($field['real_type'])) {
            $fieldType = $field['real_type'];
        }

        $uFieldName = $field['u_field'];
        $lFieldName = $field['l_field'];

        $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
        $docBlock->setTag(new Zend\Code\Generator\DocBlock\Tag\GenericTag('var', implode('|', ['null', $fieldType]) . ' ' . implode(' ', [$entity, $fieldName])));

        $property = new \Zend\Code\Generator\PropertyGenerator($lFieldName, null, \Zend\Code\Generator\PropertyGenerator::FLAG_PRIVATE);
        $property->setDocBlock($docBlock);

        $class->addPropertyFromGenerator($property);

        $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
        $docBlock->setShortDescription(implode(' ', ['Get', $l_entity, $fieldName]));
        $docBlock->setTag(new \Zend\Code\Generator\DocBlock\Tag\ReturnTag(['null', $fieldType]));

        $method = new \Zend\Code\Generator\MethodGenerator();
        $method->setDocBlock($docBlock);
        $method->setName('get' . $uFieldName);
        $method->setBody('return $this->' . strtolower($lFieldName) . ';');
        $class->addMethodFromGenerator($method);

        $docBlock = new \Zend\Code\Generator\DocBlockGenerator();
        $docBlock->setShortDescription(implode(' ', ['Set', $l_entity, $fieldName]));
        $docBlock->setTag(new \Zend\Code\Generator\DocBlock\Tag\ParamTag($lFieldName, [$fieldType]));
        $docBlock->setTag(new \Zend\Code\Generator\DocBlock\Tag\ReturnTag(['$this']));

        $method = new \Zend\Code\Generator\MethodGenerator();
        $method->setDocBlock($docBlock);
        $method->setName('set' . $uFieldName);
        $method->setParameter($lFieldName);
        $method->setBody('$this->' . $lFieldName . ' = $' . $lFieldName . ';' . PHP_EOL . PHP_EOL . 'return $this;');
        $class->addMethodFromGenerator($method);
    }

    $file = new \Zend\Code\Generator\FileGenerator();
    $file->setClass($class);

    @mkdir(implode(DIRECTORY_SEPARATOR, ['.', 'src', 'Domain', 'Entity', $entity]), 0777, true);
    file_put_contents(implode(DIRECTORY_SEPARATOR, ['.', 'src', 'Domain', 'Entity', $entity , $className . '.php']), $file->generate());
}

// Generate core
generateCoreObjects($blueprint);

// Generate entity field exceptions
generateEntityFieldExceptions($blueprint);

// Generate entity field interfaces
generateEntityFieldInterfaces($blueprint);

// Generate entity fields
generateEntityFields($blueprint);

// Generate Entity
//generateEntity($blueprint);