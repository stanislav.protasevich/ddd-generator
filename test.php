<?php

require_once 'vendor/autoload.php';

class A {
    /**
     * @var \Zend\Validator\ValidatorChain
     */
    private static $validatorChain;

    public static function addValidator(\Zend\Validator\ValidatorInterface $validator)
    {
        if (is_null(self::$validatorChain)) {
            self::$validatorChain = new \Zend\Validator\ValidatorChain();
        }

        self::$validatorChain->attach($validator);

        echo "Validator added" . PHP_EOL;
    }

    public function __construct()
    {
        echo "Constructor" . PHP_EOL;
    }
}

A::addValidator(new \Zend\Validator\NotEmpty());
$a = new A();

