<?php

require_once 'vendor/autoload.php';

define('DIR_SRC', 'membership');

define('BASE_NAMESPACE', 'Tapmedia\Membership');

define('CONTEXT_NAME_KEY', 'context');
define('NAMESPACE_NAME_KEY', 'namespace');
define('CLASS_NAME_KEY', 'class');
define('INTERFACE_NAME_KEY', 'interface');
define('FIELDS_NAME_KEY', 'fields');
define('NULLABLE_NAME_KEY', 'nullable');
define('DEFAULT_NAME_KEY', 'default');
define('USE_NAME_KEY', 'use');
define('DOMAIN_NAME_KEY', 'Domain');
define('MODEL_NAME_KEY', 'Model');

define('COLUMN_NAME', 'COLUMN_NAME');
define('REFERENCED_TABLE_NAME', 'REFERENCED_TABLE_NAME');
define('PHP', '.php');

$mapping = [
    'apple_app_store_app' => 'app_store_application',
    'google_play_market_app' => 'play_market_application',
    'mobile_app' => 'application',
    'mobile_app_language' => 'language',
    'mobile_app_translation' => 'translation',
];
$mapping = [
    'company' => 'company',
    'office' => 'office',
    'department' => 'department',
    'city' => 'city',
    'position' => 'position',
    'member' => 'member',
];

$ignors = [
//    'apple_app_store_app',
//    'google_play_market_app',
//    'mobile_app',
//    'mobile_app_language',
//    'mobile_app_translation',
//    'youtube_embedding',
];

$getConstraintsQuery = <<< EOT
SELECT
  TABLE_NAME, COLUMN_NAME, CONSTRAINT_NAME, REFERENCED_TABLE_NAME, REFERENCED_COLUMN_NAME
FROM
  INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE
  TABLE_NAME = ? AND CONSTRAINT_NAME <> 'primary' AND REFERENCED_TABLE_NAME IS NOT NULL;
EOT;

//$db = new \PDO('mysql:host=localhost;dbname=application', 'developer', 'developer');
$db = new \PDO('mysql:host=localhost;dbname=tapmedia_membership', 'root', 'root');

function getTables()
{
    global $db;
    global $mapping;
    global $ignors;

    $tables = [];

    $query = <<< EOT
    SHOW TABLES
EOT;

    $stmt = $db->query($query);

    foreach ($stmt->fetchAll(PDO::FETCH_NUM) as $row) {
        $table = array_shift($row);

        if (in_array($table, $ignors)) {
            continue;
        }

        $realTable = $table;

        if (isset($mapping[$table])) {
            $realTable = $mapping[$table];
        }

        $tables[$realTable] = $table;
    }

    return $tables;
}

function structurize(array $tables)
{
    global $db;
    global $mapping;

    $structures = [];

    $getTableCommentSql = <<< EOT
  SELECT TABLE_COMMENT as `comment`
  FROM INFORMATION_SCHEMA.TABLES
  WHERE TABLE_NAME = ?
EOT;

    foreach ($tables as $alias => $table) {
        $structure = [];

        $stmt = $db->prepare($getTableCommentSql);
        $stmt->bindParam(1, $table);
        $stmt->execute();

        $comment = json_decode($stmt->fetch(PDO::FETCH_ASSOC)['comment'], true);

        $structure[CONTEXT_NAME_KEY] = underscoreToCamel($comment['context']);
        $structure[NAMESPACE_NAME_KEY] = implode('\\', [BASE_NAMESPACE, DOMAIN_NAME_KEY, MODEL_NAME_KEY]);
        $structure[CLASS_NAME_KEY] = underscoreToCamel($alias);

        $fields = [];


        $structure[FIELDS_NAME_KEY] = $fields;
        $structure[NULLABLE_NAME_KEY] = false;

        $getConstraintsSql = <<< EOT
SELECT
  TABLE_NAME, COLUMN_NAME, CONSTRAINT_NAME, REFERENCED_TABLE_NAME, REFERENCED_COLUMN_NAME
FROM
  INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE
  TABLE_NAME = ? AND CONSTRAINT_NAME = 'primary';
EOT;

        $stmt = $db->prepare($getConstraintsSql);
        $stmt->bindParam(1, $table);
        $stmt->execute();

        $data = $stmt->fetch();

        $primary[CONTEXT_NAME_KEY] = $structure[CONTEXT_NAME_KEY];
        $primary[NAMESPACE_NAME_KEY] = implode('\\', [BASE_NAMESPACE, $structure[CONTEXT_NAME_KEY], DOMAIN_NAME_KEY, MODEL_NAME_KEY, $structure[CLASS_NAME_KEY]]);
        $primary[INTERFACE_NAME_KEY] = underscoreToCamel(implode('_', [$structure[CLASS_NAME_KEY], str_replace('id', 'identifier', $data[COLUMN_NAME]), INTERFACE_NAME_KEY]));
        $primary[CLASS_NAME_KEY] = underscoreToCamel(implode('_', [$structure[CLASS_NAME_KEY], str_replace('id', 'identifier', $data[COLUMN_NAME])]));
        $structure[FIELDS_NAME_KEY][$data[COLUMN_NAME]] = $primary;

        $getConstraintsSql = <<< EOT
SELECT
  TABLE_NAME, COLUMN_NAME, CONSTRAINT_NAME, REFERENCED_TABLE_NAME, REFERENCED_COLUMN_NAME
FROM
  INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE
  TABLE_NAME = ? AND CONSTRAINT_NAME <> 'primary';
EOT;

        $stmt = $db->prepare($getConstraintsSql);
        $stmt->bindParam(1, $table);
        $stmt->execute();

        $constraints = [];
        foreach ($stmt->fetchAll() as $constraint) {
            $temp = [];

            $stmt = $db->prepare($getTableCommentSql);
            $stmt->bindParam(1, $table);
            $stmt->execute();

            $comment = json_decode($stmt->fetch(PDO::FETCH_ASSOC)['comment'], true);

            $temp[CONTEXT_NAME_KEY] = underscoreToCamel($comment['context']);
            $temp[NAMESPACE_NAME_KEY] = implode('\\', [BASE_NAMESPACE, DOMAIN_NAME_KEY, MODEL_NAME_KEY, $structure[CLASS_NAME_KEY]]);
            $temp[INTERFACE_NAME_KEY] = underscoreToCamel(implode('_', [$constraint[COLUMN_NAME], INTERFACE_NAME_KEY]));

            $temp['reference']['table'] = null;
            if ($constraint['REFERENCED_TABLE_NAME'] !== null) {
                $temp['reference']['table'] = $constraint['REFERENCED_TABLE_NAME'];

                $temp[NAMESPACE_NAME_KEY] = implode('\\', [BASE_NAMESPACE, DOMAIN_NAME_KEY, MODEL_NAME_KEY, ucfirst($mapping[$temp['reference']['table']])]);
                $temp[INTERFACE_NAME_KEY] = implode('', [ucfirst($mapping[$temp['reference']['table']]), INTERFACE_NAME_KEY]);
            }

            $constraints[$constraint[COLUMN_NAME]] = $temp;
        }

        $structure[FIELDS_NAME_KEY] = array_merge($structure[FIELDS_NAME_KEY], $constraints);

        $stmt = $db->prepare("DESCRIBE " .  $table);
        $stmt->execute();

        foreach ($stmt->fetchAll() as $fields) {
            if (!isset($structure[FIELDS_NAME_KEY][$fields['Field']])) {
                $structure[FIELDS_NAME_KEY][$fields['Field']][NULLABLE_NAME_KEY] = $fields['Null'] == 'NO' ? false : true;

                $type = null;
                if (strpos($fields['Type'], 'tinyint') !== false) {
                    $type = 'int';
                } else if (strpos($fields['Type'], 'smallint') !== false) {
                    $type = 'int';
                } else if (strpos($fields['Type'], 'mediumint') !== false) {
                    $type = 'int';
                } else if (strpos($fields['Type'], 'bigint') !== false) {
                    $type = 'int';
                } else if (strpos($fields['Type'], 'int') !== false) {
                    $type = 'int';
                } else if (strpos($fields['Type'], 'varchar') !== false) {
                    $type = 'string';
                } else if (strpos($fields['Type'], 'char') !== false) {
                    $type = 'string';
                } else if (strpos($fields['Type'], 'text') !== false) {
                    $type = 'string';
                } else if (strpos($fields['Type'], 'double') !== false) {
                    $type = 'float';
                } else if (strpos($fields['Type'], 'float') !== false) {
                    $type = 'float';
                } else if (strpos($fields['Type'], 'datetime') !== false) {
                    $type = 'DateTime';
                } else if (strpos($fields['Type'], 'date') !== false) {
                    $type = 'DateTime';
                }

                if (is_null($type)) {
                    throw new \Exception('Unknown column type: "' . $fields['Type'] . '"');
                }

                $unsigned = false;
                if (strpos($fields['Type'], 'unsigned') !== false) {
                    $unsigned = true;
                }

                if ($fields['Default'] !== null) {
                    $value = $fields['Default'];

                    if ($type == 'int') {
                        $value = (int) $value;
                    } else if ($type == 'float') {
                        $value = (float) $value;
                    }

                    $structure[FIELDS_NAME_KEY][$fields['Field']][DEFAULT_NAME_KEY] = $value;
                }

                if ($fields['Null'] == 'YES') {
                    $structure[FIELDS_NAME_KEY][$fields['Field']][DEFAULT_NAME_KEY] = null;
                }

                $structure[FIELDS_NAME_KEY][$fields['Field']]['type'] = $type;
                $structure[FIELDS_NAME_KEY][$fields['Field']]['unsigned'] = $unsigned;
            } else {
                $structure[FIELDS_NAME_KEY][$fields['Field']][NULLABLE_NAME_KEY] = $fields['Null'] == 'NO' ? false : true;

                if ($fields['Type'] == 'char(36)') {
                    $structure[FIELDS_NAME_KEY][$fields['Field']]['type'] = 'uuid';
                } else if (strpos($fields['Type'], 'int') !== false) {
                    $structure[FIELDS_NAME_KEY][$fields['Field']]['type'] = 'int';
                } else if ($fields['Key'] == 'PRI' && strpos($fields['Type'], 'char') !== false) {
                    $structure[FIELDS_NAME_KEY][$fields['Field']]['type'] = 'sid';
                } else {
                    $structure[FIELDS_NAME_KEY][$fields['Field']]['type'] = 'string';
                }
            }
        }

        $structures[$alias] = $structure;
    }
//    var_dump($structures['app_store_application']['fields']); exit;

    return $structures;
}

/**
 * @param $string
 * @param bool $ucfirst
 * @return mixed
 */
function underscoreToCamel($string, $ucfirst = true)
{
    if ($ucfirst) {
        $string = ucfirst($string);
    }

    return \Zend\Filter\StaticFilter::execute($string, \Zend\Filter\Word\UnderscoreToCamelCase::class);
}
/**
 * @param $string
 * @return mixed
 */
function camelCaseToSeparator($string)
{
    return strtolower(\Zend\Filter\StaticFilter::execute($string, \Zend\Filter\Word\CamelCaseToSeparator::class));
}
/**
 * @param $string
 * @return mixed
 */
function camelCaseToUnderscore($string)
{
    return strtolower(\Zend\Filter\StaticFilter::execute($string, \Zend\Filter\Word\CamelCaseToUnderscore::class));
}
/**
 * @param $string
 * @return mixed
 */
function camelCaseToDash($string)
{
    return strtolower(\Zend\Filter\StaticFilter::execute($string, \Zend\Filter\Word\CamelCaseToDash::class));
}
/**
 * @param $string
 * @return mixed
 */
function underscoreToCamelCase($string)
{
    return strtolower(\Zend\Filter\StaticFilter::execute($string, \Zend\Filter\Word\UnderscoreToCamelCase::class));
}

$tables = getTables();

$structures = structurize($tables);

/* Generate Entities and Entity Interfaces */
require_once 'generators/entities.php';

/* Create Doctrine Mapper Interfaces */
require_once 'generators/mapper-interfaces.php';

/* Create Repository Interfaces */
require_once 'generators/repository-interfaces.php';

/* Create Doctrine Mappers */
require_once 'generators/mappers.php';

/* Create Repositories */
require_once 'generators/repositories.php';

/* Mapper Factories */
require_once 'generators/mapper-factories.php';

/* Repository Factories */
require_once 'generators/repository-factories.php';

/* Create Command Interface */
require_once 'generators/create-command-interface.php';

/* Create Command */
require_once 'generators/create-command.php';

/* Update Command Interface */
require_once 'generators/update-command-interface.php';

/* Update Command */
require_once 'generators/update-command.php';

/* Delete Command Interface */
require_once 'generators/delete-command-interface.php';

/* Delete Command */
require_once 'generators/delete-command.php';

/* Create Command Handler */
require_once 'generators/create-command-handler.php';

/* Update Command Handler */
require_once 'generators/update-command-handler.php';

/* Delete Command Handler */
require_once 'generators/delete-command-handler.php';

/* Create Command Handler Factory */
require_once 'generators/create-command-handler-factory.php';

/* Update Command Handler Factory */
require_once 'generators/update-command-handler-factory.php';

/* Delete Command Handler Factory */
require_once 'generators/delete-command-handler-factory.php';

/* Create Action */
require_once 'generators/create-action.php';

/* Create Action Factory */
require_once 'generators/create-action-factory.php';

/* Update Action */
require_once 'generators/update-action.php';

/* Update Action Factory */
require_once 'generators/update-action-factory.php';

/* Delete Action */
require_once 'generators/delete-action.php';

/* Delete Action Factory */
require_once 'generators/delete-action-factory.php';

/* Config Provider */
require_once 'generators/create-config-provider.php';

/* Routes */
require_once 'generators/routes.php';
